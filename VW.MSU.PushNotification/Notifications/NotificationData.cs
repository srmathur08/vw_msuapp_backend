﻿namespace Skoda.PCMP.PushNotification.Notifications
{
    public abstract class NotificationData
    {
    }

    public class JeepLifeNotificationData : NotificationData
    {
        public int NotificationId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ModuleType { get; set; }
        public long ModuleId { get; set; }
        public string Image { get; set; }
        public long SubModuleId { get; set; }
        public string Action { get; set; }
    }
}
