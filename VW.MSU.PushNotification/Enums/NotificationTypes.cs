﻿namespace Skoda.PCMP.PushNotification.Enums
{
    public enum NotificationTypes
    {
        CreateTask = 1,
        TaskCompletion = 2,
        UnrelatedTask = 3,
        ForceCloseTask = 4
    }
}
