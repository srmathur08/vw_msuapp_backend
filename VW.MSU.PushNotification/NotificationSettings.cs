﻿namespace Skoda.PCMP.PushNotification
{
    /// <summary>
    /// Notification settings for devices
    /// </summary>
    public interface INotificationSettings { }

    /// <summary>
    /// Settings for Devices
    /// </summary>
    public class NotificationSettings : INotificationSettings
    {
        /// <summary>
        /// Api key to authenticate
        /// </summary>
        public string ApiKey { get; set; }
        /// <summary>
        /// Application Number when registering
        /// </summary>
        public string SenderId { get; set; }
        /// <summary>
        /// Google Messaging Endpoint
        /// </summary>
        public string RequestUrl { get; set; }

        public string Priority { get; set; } = "normal";
    }
}