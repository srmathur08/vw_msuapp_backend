﻿using System;

namespace Skoda.PCMP.PushNotification
{
    public class NotificationResponse
    {
        public static readonly string FAILURE = "failure";
        public static readonly string SUCCESS = "success";

        public int NotificationId { get; set; }
        public string Result { get; set; } = SUCCESS;
        public string Message { get; set; } = "";
        public string Data { get; set; } = "";

        public static void GrabException(NotificationResponse response, Exception ex)
        {
            response.Result = FAILURE;
            response.Message = ex.ToString();
        }

        public static NotificationResponse GetFailedResponse(NotificationResponse response, string errorMessage)
        {
            response.Result = FAILURE;
            response.Message = errorMessage;

            return response;
        }

        public static NotificationResponse GetFailedResponse(string errorMessage)
        {
            return new NotificationResponse()
            {
                Result = FAILURE,
                Message = errorMessage
            };
        }
    }
}