﻿using Skoda.PCMP.PushNotification.Notifications;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;

namespace Skoda.PCMP.PushNotification
{
    public class NotificationHandler
    {
        public string[] RegistrationIds { get; set; }
        public GcmNotificationData Notification { get; set; }
        public string Priority { get; set; }

        public static async Task<NotificationResponse> PushNotificationAsync(string[] registrationIds, NotificationData data, string ApiKey, string SenderId, string RequestUrl, string Priority, string title, string body = "", string image = "", int notificationId = 0)
        {
            return await PushNotificationAsync(data, new NotificationHandler()
            {
                RegistrationIds = registrationIds,
                Notification = new GcmNotificationData()
                {
                    Title = title,
                    Body = body,
                    Image = image
                },
                Priority = ""
            },
            new NotificationSettings
            {
                ApiKey = ApiKey,
                Priority = Priority,
                RequestUrl = RequestUrl,
                SenderId = SenderId
            },
            notificationId);
        }

        public static async Task<NotificationResponse> PushNotificationAsync(NotificationData data, NotificationHandler notification, NotificationSettings notificationSettings, int notificationId = 0)
        {
            NotificationResponse nResponse = new NotificationResponse
            {
                NotificationId = notificationId
            };

            if (string.IsNullOrWhiteSpace(notification.Priority))
                notification.Priority = notificationSettings.Priority;

            GcmPostFields fields = new GcmPostFields
            {
                Registration_ids = notification.RegistrationIds,
                Notification = notification.Notification,
                Priority = notification.Priority,
                Data = data
            };

            ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(PushNotification.ValidateServerCertificate);
            // content
            string content = JsonConvert.SerializeObject(
                                fields, 
                                new JsonSerializerSettings
                                {
                                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                                });
            byte[] byteArray = Encoding.UTF8.GetBytes(content);

            // prepare Request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(notificationSettings.RequestUrl);
            request.Method = "POST";
            request.KeepAlive = false;
            request.ContentType = "application/json";
            request.Headers.Add($"Authorization: key={notificationSettings.ApiKey}");
            request.Headers.Add($"Sender: id={notificationSettings.SenderId}");
            request.ContentLength = byteArray.Length;

            //  SEND request  
            try
            {
                using Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                using WebResponse response = await request.GetResponseAsync();
                HttpStatusCode ResponseCode = ((HttpWebResponse)response).StatusCode;
                if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
                {
                    NotificationResponse.GetFailedResponse(nResponse, "Unauthorized - need new token");
                }
                else if (!ResponseCode.Equals(HttpStatusCode.OK))
                {
                    NotificationResponse.GetFailedResponse(nResponse, "Response from web service isn't OK");
                }

                using StreamReader reader = new StreamReader(response.GetResponseStream());
                nResponse.Message = await reader.ReadToEndAsync();
            }
            catch (WebException ex)
            {
                if (ex.Response is HttpWebResponse)
                {
                    HttpStatusCode ResponseCode = ((HttpWebResponse)ex.Response).StatusCode;
                    if (ResponseCode.Equals(HttpStatusCode.Unauthorized))
                    {
                        NotificationResponse.GetFailedResponse(nResponse, "Unauthorized - need new token");
                    }
                    else if (ResponseCode.Equals(HttpStatusCode.Forbidden))
                    {
                        NotificationResponse.GetFailedResponse(nResponse, "Forbidden");
                    }
                    else if (!ResponseCode.Equals(HttpStatusCode.OK))
                    {
                        NotificationResponse.GetFailedResponse(nResponse, "Response from web service isn't OK");
                    }
                }
                else
                    throw;
            }
            catch (Exception ex)
            {
                NotificationResponse.GrabException(nResponse, ex);
            }

            return nResponse;
        }

        private class GcmPostFields
        {
            public string[] Registration_ids { get; set; }
            public GcmNotificationData Notification { get; set; }
            public string Priority { get; set; }
            public NotificationData Data { get; set; }
        }
    }

    public class GcmNotificationData
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string Image { get; set; }
    }
}