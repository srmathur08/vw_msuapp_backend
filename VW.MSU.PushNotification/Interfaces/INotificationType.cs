﻿using Skoda.PCMP.PushNotification.Enums;

namespace Skoda.PCMP.PushNotification.Interfaces
{
    public interface INotificationType
    {
        NotificationTypes NotificationType { get; set; }
    }

}
