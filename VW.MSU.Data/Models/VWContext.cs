﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace VW.MSU.Data.Models
{
    public partial class VWContext : DbContext
    {
        public VWContext()
        {
        }

        public VWContext(DbContextOptions<VWContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cmsroles> Cmsroles { get; set; }
        public virtual DbSet<Dealership> Dealership { get; set; }
        public virtual DbSet<GroupDealer> GroupDealer { get; set; }
        public virtual DbSet<GroupDealerUsers> GroupDealerUsers { get; set; }
        public virtual DbSet<Models> Models { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<ServiceRequest> ServiceRequest { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<VehicleCheckup> VehicleCheckup { get; set; }
        public virtual DbSet<VehicleCheckupAsset> VehicleCheckupAsset { get; set; }
        public virtual DbSet<VehicleCheckupData> VehicleCheckupData { get; set; }
        public virtual DbSet<VehicleCheckupDataAsset> VehicleCheckupDataAsset { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-DGT4J9U\\SQLEXPRESS;Initial Catalog=VWMSU_DEV;User Id=sa;Password=abc;MultipleActiveResultSets=True;Persist Security Info=False;");
                //optionsBuilder.UseSqlServer("Data Source=VF-HYD-SQL-01;Initial Catalog=VWMSU_DEV;User Id=skoda;Password=Skoda@123;MultipleActiveResultSets=True;Persist Security Info=False;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cmsroles>(entity =>
            {
                entity.HasKey(e => e.CmsroleId);

                entity.ToTable("CMSRoles");

                entity.Property(e => e.CmsroleId).HasColumnName("CMSRoleId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<Dealership>(entity =>
            {
                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.AdminEmail)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.AdminFullName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.AdminMobile)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DealershipType)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.Image).HasMaxLength(100);

                entity.Property(e => e.Landline)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Location)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Mobile)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Stdcode)
                    .IsRequired()
                    .HasColumnName("STDCode")
                    .HasMaxLength(30);

                entity.HasOne(d => d.AreaManager)
                    .WithMany(p => p.Dealership)
                    .HasForeignKey(d => d.AreaManagerId)
                    .HasConstraintName("FK_Dealership_Users");

                entity.HasOne(d => d.GroupDealer)
                    .WithMany(p => p.Dealership)
                    .HasForeignKey(d => d.GroupDealerId)
                    .HasConstraintName("FK_Dealership_GroupDealer");
            });

            modelBuilder.Entity<GroupDealer>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<GroupDealerUsers>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Fullname)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.GroupDealer)
                    .WithMany(p => p.GroupDealerUsers)
                    .HasForeignKey(d => d.GroupDealerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupDealerUsers_GroupDealer");
            });

            modelBuilder.Entity<Models>(entity =>
            {
                entity.Property(e => e.BackAngleImage).HasMaxLength(200);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FrontAngleImage).HasMaxLength(200);

                entity.Property(e => e.Image).HasMaxLength(200);

                entity.Property(e => e.LeftAngleImage).HasMaxLength(200);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.RightAngleImage).HasMaxLength(200);
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<ServiceRequest>(entity =>
            {
                entity.HasKey(e => e.Srid);

                entity.Property(e => e.Srid).HasColumnName("SRId");

                entity.Property(e => e.CompleteTime).HasColumnType("datetime");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Duration).HasMaxLength(50);

                entity.Property(e => e.InvoiceAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Lat).HasMaxLength(50);

                entity.Property(e => e.Lng).HasMaxLength(50);

                entity.Property(e => e.OdometerReading).HasMaxLength(20);

                entity.Property(e => e.RegNum).HasMaxLength(50);

                entity.Property(e => e.ServiceDateTime).HasColumnType("datetime");

                entity.Property(e => e.ServiceType).HasMaxLength(100);

                entity.Property(e => e.Srnumber)
                    .IsRequired()
                    .HasColumnName("SRNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.VariantName).HasMaxLength(50);

                entity.Property(e => e.VehicleModel).HasMaxLength(50);

                entity.Property(e => e.VehicleModelId).HasDefaultValueSql("((0))");

                entity.Property(e => e.Vinnumber)
                    .IsRequired()
                    .HasColumnName("VINNumber")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Dealership)
                    .WithMany(p => p.ServiceRequest)
                    .HasForeignKey(d => d.DealershipId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ServiceRequest_Dealership");

                entity.HasOne(d => d.VehicleModelNavigation)
                    .WithMany(p => p.ServiceRequest)
                    .HasForeignKey(d => d.VehicleModelId)
                    .HasConstraintName("FK_ServiceRequest_Models");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.CmsroleId).HasColumnName("CMSRoleId");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Fullname)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LastLogin).HasColumnType("datetime");

                entity.Property(e => e.Password).HasMaxLength(100);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.Cmsrole)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.CmsroleId)
                    .HasConstraintName("FK_Users_CMSRoles");

                entity.HasOne(d => d.DealershipNavigation)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.DealershipId)
                    .HasConstraintName("FK_Users_Dealership");

                entity.HasOne(d => d.GroupDealer)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.GroupDealerId)
                    .HasConstraintName("FK_Users_GroupDealer");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_Users_Roles");
            });

            modelBuilder.Entity<VehicleCheckup>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FuelLevel).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Srid).HasColumnName("SRId");

                entity.Property(e => e.VehicleMileage).HasMaxLength(50);

                entity.HasOne(d => d.Sr)
                    .WithMany(p => p.VehicleCheckup)
                    .HasForeignKey(d => d.Srid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VehicleCheckup_ServiceRequest");
            });

            modelBuilder.Entity<VehicleCheckupAsset>(entity =>
            {
                entity.Property(e => e.Asset).HasMaxLength(200);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Thumb).HasMaxLength(200);

                entity.HasOne(d => d.VehicleCheckup)
                    .WithMany(p => p.VehicleCheckupAsset)
                    .HasForeignKey(d => d.VehicleCheckupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VehicleCheckupAsset_VehicleCheckup");
            });

            modelBuilder.Entity<VehicleCheckupData>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Remarks).HasMaxLength(200);

                entity.Property(e => e.SectionName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.VehicleCheckup)
                    .WithMany(p => p.VehicleCheckupData)
                    .HasForeignKey(d => d.VehicleCheckupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VehicleCheckupData_VehicleCheckup");
            });

            modelBuilder.Entity<VehicleCheckupDataAsset>(entity =>
            {
                entity.Property(e => e.Asset).HasMaxLength(200);

                entity.Property(e => e.Thumb).HasMaxLength(200);

                entity.HasOne(d => d.VehicleCheckupData)
                    .WithMany(p => p.VehicleCheckupDataAsset)
                    .HasForeignKey(d => d.VehicleCheckupDataId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VehicleCheckupDataAsset_VehicleCheckupData");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
