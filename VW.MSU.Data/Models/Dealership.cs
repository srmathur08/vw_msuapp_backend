﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Data.Models
{
    public partial class Dealership
    {
        public Dealership()
        {
            ServiceRequest = new HashSet<ServiceRequest>();
            Users = new HashSet<Users>();
        }

        public int DealershipId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string Stdcode { get; set; }
        public string Landline { get; set; }
        public string Mobile { get; set; }
        public string AdminFullName { get; set; }
        public string AdminEmail { get; set; }
        public string AdminMobile { get; set; }
        public string DealershipType { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public int? GroupDealerId { get; set; }
        public int? AreaManagerId { get; set; }
        public string Image { get; set; }

        public virtual Users AreaManager { get; set; }
        public virtual GroupDealer GroupDealer { get; set; }
        public virtual ICollection<ServiceRequest> ServiceRequest { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
