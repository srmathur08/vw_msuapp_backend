﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Data.Models
{
    public partial class VehicleCheckupAsset
    {
        public long Id { get; set; }
        public long VehicleCheckupId { get; set; }
        public string Asset { get; set; }
        public string Thumb { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual VehicleCheckup VehicleCheckup { get; set; }
    }
}
