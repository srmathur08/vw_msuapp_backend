﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Data.Models
{
    public partial class Models
    {
        public Models()
        {
            ServiceRequest = new HashSet<ServiceRequest>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string FrontAngleImage { get; set; }
        public string BackAngleImage { get; set; }
        public string LeftAngleImage { get; set; }
        public string RightAngleImage { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<ServiceRequest> ServiceRequest { get; set; }
    }
}
