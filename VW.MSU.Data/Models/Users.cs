﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Data.Models
{
    public partial class Users
    {
        public Users()
        {
            Dealership = new HashSet<Dealership>();
        }

        public int UserId { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? RoleId { get; set; }
        public DateTime? LastLogin { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int? DealershipId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public string Password { get; set; }
        public int? CmsroleId { get; set; }
        public int? GroupDealerId { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public DateTime? LocModifyDate { get; set; }

        public virtual Cmsroles Cmsrole { get; set; }
        public virtual Dealership DealershipNavigation { get; set; }
        public virtual GroupDealer GroupDealer { get; set; }
        public virtual Roles Role { get; set; }
        public virtual ICollection<Dealership> Dealership { get; set; }
    }
}
