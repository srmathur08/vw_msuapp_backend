﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Data.Models
{
    public partial class GroupDealerUsers
    {
        public long Id { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int GroupDealerId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }

        public virtual GroupDealer GroupDealer { get; set; }
    }
}
