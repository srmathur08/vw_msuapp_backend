﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Data.Models
{
    public partial class VehicleCheckupData
    {
        public VehicleCheckupData()
        {
            VehicleCheckupDataAsset = new HashSet<VehicleCheckupDataAsset>();
        }

        public long Id { get; set; }
        public long VehicleCheckupId { get; set; }
        public string SectionName { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Remarks { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Status { get; set; }

        public virtual VehicleCheckup VehicleCheckup { get; set; }
        public virtual ICollection<VehicleCheckupDataAsset> VehicleCheckupDataAsset { get; set; }
    }
}
