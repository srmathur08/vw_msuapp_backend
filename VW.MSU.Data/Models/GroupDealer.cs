﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Data.Models
{
    public partial class GroupDealer
    {
        public GroupDealer()
        {
            Dealership = new HashSet<Dealership>();
            GroupDealerUsers = new HashSet<GroupDealerUsers>();
            Users = new HashSet<Users>();
        }

        public int GroupDealerId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Dealership> Dealership { get; set; }
        public virtual ICollection<GroupDealerUsers> GroupDealerUsers { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
