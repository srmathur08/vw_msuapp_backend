﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Data.Models
{
    public partial class Cmsroles
    {
        public Cmsroles()
        {
            Users = new HashSet<Users>();
        }

        public int CmsroleId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Users> Users { get; set; }
    }
}
