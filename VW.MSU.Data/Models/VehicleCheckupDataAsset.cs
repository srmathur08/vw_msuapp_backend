﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Data.Models
{
    public partial class VehicleCheckupDataAsset
    {
        public long Id { get; set; }
        public long VehicleCheckupDataId { get; set; }
        public string Asset { get; set; }
        public string Thumb { get; set; }

        public virtual VehicleCheckupData VehicleCheckupData { get; set; }
    }
}
