﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Data.Models
{
    public partial class VehicleCheckup
    {
        public VehicleCheckup()
        {
            VehicleCheckupAsset = new HashSet<VehicleCheckupAsset>();
            VehicleCheckupData = new HashSet<VehicleCheckupData>();
        }

        public long Id { get; set; }
        public long Srid { get; set; }
        public decimal? FuelLevel { get; set; }
        public string VehicleMileage { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsCompleted { get; set; }

        public virtual ServiceRequest Sr { get; set; }
        public virtual ICollection<VehicleCheckupAsset> VehicleCheckupAsset { get; set; }
        public virtual ICollection<VehicleCheckupData> VehicleCheckupData { get; set; }
    }
}
