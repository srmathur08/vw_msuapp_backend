﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Data.Models
{
    public partial class ServiceRequest
    {
        public ServiceRequest()
        {
            VehicleCheckup = new HashSet<VehicleCheckup>();
        }

        public long Srid { get; set; }
        public string Srnumber { get; set; }
        public string Vinnumber { get; set; }
        public string CustomerName { get; set; }
        public string VehicleModel { get; set; }
        public string VariantName { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public string Duration { get; set; }
        public bool IsOilChange { get; set; }
        public bool IsBrakes { get; set; }
        public bool IsTyreCheck { get; set; }
        public bool IsBatteryCheck { get; set; }
        public DateTime ServiceDateTime { get; set; }
        public int DealershipId { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public bool IsCompleted { get; set; }
        public string RegNum { get; set; }
        public string OdometerReading { get; set; }
        public string ServiceType { get; set; }
        public long? VehicleModelId { get; set; }
        public bool IsStarted { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? CompleteTime { get; set; }
        public long? Assignee { get; set; }

        public virtual Dealership Dealership { get; set; }
        public virtual Models VehicleModelNavigation { get; set; }
        public virtual ICollection<VehicleCheckup> VehicleCheckup { get; set; }
    }
}
