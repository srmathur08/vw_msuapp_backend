﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VW.MSU.Common.Attributes;

namespace VW.MSU.Models
{
    public partial class DealershipModel
    {
        public DealershipModel()
        {
        }

        public int DealershipId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Location { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        [MaxLength(3)]
        [MinLength(1)]
        public string Stdcode { get; set; }
        [Required]
        public string Landline { get; set; }
        [Required]
        public string Mobile { get; set; }
        [Required]
        public string AdminFullName { get; set; }
        [Required]
        public string AdminEmail { get; set; }
        [Required]
        public string AdminMobile { get; set; }
        [Required]
        public string DealershipType { get; set; }
        public int GroupDealerId { get; set; }
        [Required]
        public int AreaManagerId { get; set; }
        public string Image { get; set; }
        [RequiredIf(nameof(DealershipId), ErrorMessage = "Image is required")]
        [AllowedExtensions(new string[] { ".jpg", ".png" }, ErrorMessage = "Only Image files allowed.")]
        public IFormFile ImageBlob { get; set; }
        public bool IsActive { get; set; }
        public List<SelectListItem> GroupDealers { get; set; }
        public List<SelectListItem> AreaManagers { get; set; }
        public string ErrorMessage { get; set; }
    }
}
