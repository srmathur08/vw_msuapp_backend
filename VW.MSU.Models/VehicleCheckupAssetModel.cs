﻿namespace VW.MSU.Models
{
    public partial class VehicleCheckupAssetModel
    {
        public long Id { get; set; }
        public long VehicleCheckupId { get; set; }
        public string Asset { get; set; }
        public string Thumb { get; set; }
        public bool IsLastChunk { get; set; }
        public string AssetBase64String { get; set; }
        public bool IsVideo { get; set; }
    }
}
