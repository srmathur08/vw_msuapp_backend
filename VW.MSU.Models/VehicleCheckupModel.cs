﻿using System.Collections.Generic;

namespace VW.MSU.Models
{
    public partial class VehicleCheckupModel
    {
        public long Id { get; set; }
        public long Srid { get; set; }
        public decimal? FuelLevel { get; set; }
        public string VehicleMileage { get; set; }
    }

    public partial class ResponseVehicleCheckupModel
    {
        public long Id { get; set; }
        public long Srid { get; set; }
        public decimal? FuelLevel { get; set; }
        public string VehicleMileage { get; set; }
        public bool IsCompleted { get; set; }
        public List<VehicleCheckupAssetModel> VehicleCheckupAssets { get; set; }
        public List<ResponseVehicleCheckupDataModel> VehicleCheckupData { get; set; }
    }

    public partial class ResponseVehicleCheckupDataModel
    {
        public long Id { get; set; }
        public long VehicleCheckupId { get; set; }
        public string SectionName { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Remarks { get; set; }
        public int Status { get; set; }
        public List<VehicleCheckupDataAssetModel> Assets { get; set; }
    }
}
