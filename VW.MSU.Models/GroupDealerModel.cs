﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VW.MSU.Models
{
    public partial class GroupDealerModel
    {
        public GroupDealerModel()
        {
            GroupDealerUsers = new List<GroupDealerUserModel>();
        }

        public int GroupDealerId { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Dealership Count")]
        public int DealershipCount { get; set; }
        public bool IsActive { get; set; }
        public string ErrorMessage { get; set; }

        public List<GroupDealerUserModel> GroupDealerUsers { get; set; }
    }
}
