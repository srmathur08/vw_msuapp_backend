﻿using System;
using System.Collections.Generic;

namespace VW.MSU.Models
{
    public partial class GroupDealerUserModel
    {
        public long Id { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsDeleted { get; set; }
    }
}
