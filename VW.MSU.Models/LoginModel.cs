﻿using System.ComponentModel.DataAnnotations;

namespace VW.MSU.Models
{
    public class LoginModel
    {
        [Required]
        public string PhoneNo { get; set; }
        [Required]
        public string Password { get; set; }
        public string ErrorMessage { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
}
