﻿namespace VW.MSU.Models
{
    public partial class AppendAssetModel
    {
        public long AssociateId { get; set; }
        public string AssetPath { get; set; }
        public string Asset { get; set; }
        public bool IsLastChunk { get; set; } = false;
    }
}
