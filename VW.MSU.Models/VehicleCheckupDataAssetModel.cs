﻿namespace VW.MSU.Models
{
    public partial class VehicleCheckupDataAssetModel
    {
        public long Id { get; set; }
        public long VehicleCheckupDataId { get; set; }
        public string Asset { get; set; }
        public string Thumb { get; set; }
        public bool IsLastChunk { get; set; }
        public string AssetBase64String { get; set; }
        public bool IsVideo { get; set; }
    }
}
