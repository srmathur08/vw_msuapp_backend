﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VW.MSU.Common.Attributes;

namespace VW.MSU.Models
{
    public partial class UserModel
    {
        public UserModel()
        {
        }

        public int UserId { get; set; }
        [Required]
        public string Fullname { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
        public int? RoleId { get; set; }
        public DateTime? LastLogin { get; set; }
        public bool IsActive { get; set; }
        public int? DealershipId { get; set; }
        [RequiredIf(nameof(UserId))]
        public string Password { get; set; }
        [RequiredIf(nameof(UserId))]
        [Display(Name = "Confirm Password")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public int? CmsroleId { get; set; }
        public int? GroupDealerId { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public List<SelectListItem> GroupDealers { get; set; }
        public List<SelectListItem> Roles { get; set; }
        public List<SelectListItem> CMSRoles { get; set; }
        public List<SelectListItem> Dealerships { get; set; }
        public string ErrorMessage { get; set; }
    }
}
