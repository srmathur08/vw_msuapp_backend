﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using VW.MSU.Common.Attributes;

namespace VW.MSU.Models
{
    public partial class ModelsModel
    {
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Image { get; set; }
        [Display(Name = "Model Image")]
        [RequiredIf(nameof(Id), ErrorMessage = "Image is required")]
        [AllowedExtensions(new string[] { ".jpg", ".png" }, ErrorMessage = "Only Image files allowed.")]
        public IFormFile ImageBlob { get; set; }
        public string FrontAngleImage { get; set; }
        public string BackAngleImage { get; set; }
        public string LeftAngleImage { get; set; }
        public string RightAngleImage { get; set; }
        [Display(Name = "Front Angle Image")]
        [RequiredIf(nameof(Id), ErrorMessage = "Front Angle Image is required")]
        [AllowedExtensions(new string[] { ".jpg", ".png" }, ErrorMessage = "Only Image files allowed.")]
        public IFormFile FrontAngleImageBlob { get; set; }
        [Display(Name = "Back Angle Image")]
        [RequiredIf(nameof(Id), ErrorMessage = "Back Angle Image is required")]
        [AllowedExtensions(new string[] { ".jpg", ".png" }, ErrorMessage = "Only Image files allowed.")]
        public IFormFile BackAngleImageBlob { get; set; }
        [Display(Name = "Left Angle Image")]
        [RequiredIf(nameof(Id), ErrorMessage = "Left Angle Image is required")]
        [AllowedExtensions(new string[] { ".jpg", ".png" }, ErrorMessage = "Only Image files allowed.")]
        public IFormFile LeftAngleImageBlob { get; set; }
        [Display(Name = "Right Angle Image")]
        [RequiredIf(nameof(Id), ErrorMessage = "Right Angle Image is required")]
        [AllowedExtensions(new string[] { ".jpg", ".png" }, ErrorMessage = "Only Image files allowed.")]
        public IFormFile RightAngleImageBlob { get; set; }
        public string ErrorMessage { get; set; }
    }
}
