﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VW.MSU.Models
{
    [Serializable]
    public class UserDetails
    {
        public string Fullname { get; set; }
        public string PhoneNo { get; set; }
        public string Password { get; set; }
        public int UserId { get; set; }
        public int? RoleId { get; set; }
        public string Role { get; set; }
        public int? CMSRoleId { get; set; }
        public string CMSRole { get; set; }
        public int DealershipId { get; set; }
        public int GroupDealerId { get; set; }
        public bool IsDeleted { get; set; }
        public string ErrorMessage { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public DateTime? LocModifyDate { get; set; }
    }
}