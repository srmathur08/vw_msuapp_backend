﻿using System.Collections.Generic;

namespace VW.MSU.Models
{
    public partial class VehicleCheckupDataModel
    {
        public long Id { get; set; }
        public long VehicleCheckupId { get; set; }
        public string SectionName { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Remarks { get; set; }
        public int Status { get; set; }
    }

    public partial class ReqVehicleCheckupDataArrayModel
    {
        public long VehicleCheckupId { get; set; }
        public List<VehicleCheckupDataModel> CheckupData { get; set; }
    }

    public partial class ResVehicleCheckupDataModel
    {
        public long VehicleCheckupDataId { get; set; }
        public string SectionName { get; set; }
        public string Name { get; set; }
    }
}
