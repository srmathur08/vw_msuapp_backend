﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VW.MSU.Models
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>")]
    public class VinDataModel
    {
        public VehicleMaster vehicleMasterData { get; set; }
        public ServiceHistoryData Service_History_Data { get; set; }
        public string Overall_customer_satisfaction_score { get; set; }
        public string Message { get; set; }
        public DealerPSFQuestion Dealer_PSF_Question_rated_less_than_4_star { get; set; }
        public CustomerMaster customerMasterData { get; set; }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>")]
    public class VehicleMaster {
        public Attributes attributes { get; set; }
        public string Id { get; set; }
        public string VW_ModelDesc__c { get; set; }
        public string Product__c { get; set; }
        public string Variant_name__c { get; set; }
        public string VW_ModelYear__c { get; set; }
        public Product Product__r { get; set; }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>")]
    public class CustomerMaster
    {
        public Attributes attributes { get; set; }
        public string Id { get; set; }
        public string VW_CustomerName__c { get; set; }
        public string VW_First_name__c { get; set; }
        public string VW_Last_Name__c { get; set; }
        public string VW_End_User_Mobile__c { get; set; }
        public string VW_Primary_Email_ID__c { get; set; }
        public bool VW_Active__c { get; set; }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>")]
    public class Attributes
    {
        public string type { get; set; }
        public string url { get; set; }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>")]
    public class Product {
        public Attributes attributes { get; set; }
        public string Id { get; set; }
        public string Grade_Description__c { get; set; }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>")]
    public class ServiceHistoryData {
        public Attributes attributes { get; set; }
        public string Id { get; set; }
        public string CreatedDate { get; set; }
        public string VW_VIN__c { get; set; }
        public string VW_Registration_No__c { get; set; }
        public string RO_Closed_Date__c { get; set; }
        public string RO_Type__c { get; set; }
        public string Delivery_Date_Nadcon__c { get; set; }
        public long Mileage_Out__c { get; set; }
        public string Selling_Dealer__c { get; set; }
        public string Service_Dealer__c { get; set; }
		public string Selling_Dealer_Code__c { get; set; }
        public string Post_Service_feedback_for_VW_HQ__c { get; set; }
        public string VW_Brand__c { get; set; }
        public string RecordTypeId { get; set; }
        public SellingDealer Selling_Dealer__r { get; set; }
        public ServiceDealer Service_Dealer__r { get; set; }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>")]
    public class SellingDealer
    {
        public Attributes attributes { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>")]
    public class ServiceDealer
    {
        public Attributes attributes { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>")]
    public class DealerPSFQuestion
    { 
        public string test3 { get; set; }
		public string test2 { get; set; }
        public string test1 { get; set; }
    }
}
