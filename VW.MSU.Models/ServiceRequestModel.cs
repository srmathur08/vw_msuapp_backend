﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace VW.MSU.Models
{
    public partial class ServiceRequestModel
    {
        public ServiceRequestModel()
        {
        }

        public long Srid { get; set; }
        [Required]
        [Display(Name = "SR Number")]
        public string Srnumber { get; set; }
        [Required]
        [Display(Name = "VIN Number")]
        public string Vinnumber { get; set; }
        [Required]
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
        [Display(Name = "Vehicle Model")]
        public string VehicleModel { get; set; }
        public string VariantName { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public string Duration { get; set; }
        public bool IsOilChange { get; set; }
        public bool IsBrakes { get; set; }
        public bool IsTyreCheck { get; set; }
        public bool IsBatteryCheck { get; set; }

        public DateTime ServiceDateTime;
        private string _ServiceDate;
        [Required]
        [Display(Name = "Service Date & Time")]
        public string ServiceDate
        {
            get
            {
                return _ServiceDate;
            }
            set
            {
                _ServiceDate = value;
                if (DateTime.TryParseExact(value, "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out ServiceDateTime)) { }
                else if (DateTime.TryParseExact(value, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out ServiceDateTime)) { }
                else if (DateTime.TryParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ServiceDateTime)) { }
            }
        }

        public int DealershipId { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public bool IsStarted { get; set; }
        public bool IsCompleted { get; set; }
        [Display(Name = "Registration Number")]
        [Required]
        public string RegNum { get; set; }
        [Display(Name = "Odometer Reading")]
        [Required]
        public string OdometerReading { get; set; }
        [Display(Name = "Service Type")]
        [Required]
        public string ServiceType { get; set; }
        [Display(Name = "Model")]
        [Required, Range(1, int.MaxValue, ErrorMessage = "Model is Required")]
        public long VehicleModelId { get; set; }
        public List<SelectListItem> Models { get; set; }
        public string VehicleImage { get; set; }
        public List<SelectListItem> Dealerships { get; set; }
        public int? GroupDealerId { get; set; }
        public List<SelectListItem> GroupDealers { get; set; }
        public bool IsVCCompleted { get; set; }
        public string ErrorMessage { get; set; }
        public long? Assignee { get; set; }
        public ModelsModel VehicleModelData { get; set; }
        public ResponseVehicleCheckupModel VehicleCheckup { get; set; }
        public List<VehicleCheckupSecData> VehicleCheckupSecData { get; set; }
        public List<SelectListItem> ServiceAdvisers { get; set; }
    }

    public class VehicleCheckupSecData
    {
        public string SectionName { get; set; }
        public string SectionNameKey { get; set; }
        public int Percent { get; set; }
        public List<ResponseVehicleCheckupDataModel> VehicleCheckupData { get; set; }
    }

    public class VehicleCheckupAngleData
    {
        public double x { get; set; }
        public double y { get; set; }
        public string mark { get; set; }
    }

    public class ServiceRequestListModel
    { 
        public List<ServiceRequestModel> ServiceRequests { get; set; }
        public DateTime ServiceDate { get; set; }
        public int TotalSRs { get; set; }
        public UserModel AssigneeDetails{ get; set; }
    }
}
