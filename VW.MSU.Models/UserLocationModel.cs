﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VW.MSU.Models
{
    [Serializable]
    public class UserLocationModel
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public string Lat { get; set; }
        [Required]
        public string Lng { get; set; }
    }
}
