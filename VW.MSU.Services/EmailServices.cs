﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace VW.MSU.Services
{
    public class EmailServices
    {
        public static void SendMail(string Host, int Port, string Username, string Password, bool EnableSsl,
            string FromAddress, string FromName, string ToAddress,
            string Body, string Subject, bool IsBodyHtml, SendCompletedEventHandler callback)
        {
            try
            {
                SmtpClient client = new SmtpClient(Host);
                client.EnableSsl = EnableSsl;
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                if (Port > 0)
                    client.Port = Port;

                if (!string.IsNullOrEmpty(Username))
                    client.Credentials = new NetworkCredential(Username, Password);

                MailAddress from = new MailAddress(FromAddress, FromName, System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress(ToAddress);

                MailMessage message = new MailMessage();
                message.From = from;
                message.To.Add(to);
                message.Body = Body;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Subject = Subject;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = IsBodyHtml;

                if (callback != null)
                    client.SendCompleted += callback;

                client.Send(message);
            }
            catch (Exception ex)
            {
                throw (ex);      
            }
        }
    }
}
