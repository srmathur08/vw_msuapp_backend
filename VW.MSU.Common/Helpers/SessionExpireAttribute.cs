﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;

namespace VW.MSU.Common
{
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ISession session = filterContext.HttpContext.Session;

            if (filterContext.Controller is Controller)
            {
                var ctrl = ((ControllerBase)filterContext.Controller).ControllerContext.ActionDescriptor.ControllerName;
                var action = ((ControllerBase)filterContext.Controller).ControllerContext.ActionDescriptor.ActionName;

                if ((ctrl == "Home" && action == "SR") || (ctrl == "Home" && action == "Login") || (ctrl == "Home" && action == "ForgotPasswordConfirmation") || (ctrl == "Home" && action == "Forgotpassword"))
                { }
                else
                {
                    if (session != null && !session.TryGetValue("UserDetails", out _))
                    {
                        filterContext.Result =
                               new RedirectToRouteResult(
                                   new RouteValueDictionary{{ "controller", "Home" },
                                          { "action", "Login" }
                                });
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}