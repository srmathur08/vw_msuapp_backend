﻿namespace VW.MSU.Common.Enums
{
    public enum CMSRoleTypes : int
    {
        SUPERADMIN = 1,
        VWADMIN = 2,
        GROUPDEALERADMIN = 3,
        DEALERSHIPADMIN = 4,
        NSCUSER = 5,
        REGUSER = 6
    }
}