﻿namespace VW.MSU.Common.Enums
{
    public enum RoleTypes : int
    {
        AREAMANAGER = 1,
        ZONALMANAGER = 2,
        HO = 3
    }
}