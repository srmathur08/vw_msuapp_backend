﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace VW.MSU.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class RequiredIfAttribute : ValidationAttribute, IClientModelValidator
    {
        public string PropertyName { get; private set; }

        public RequiredIfAttribute(string PropertyName)
        {
            this.PropertyName = PropertyName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is IFormFile)
            {
                if (((IFormFile)value).Length > 0)
                    return ValidationResult.Success;
            }
            else
            {
                var valueStr = value as string;
                if (!string.IsNullOrWhiteSpace(valueStr))
                    return ValidationResult.Success;
            }

            PropertyInfo PropertyInfo = validationContext.ObjectType.GetRuntimeProperty(PropertyName); //Added

            object PropertyValue = PropertyInfo.GetValue(validationContext.ObjectInstance, null);
            if (PropertyValue == null) return new ValidationResult(ErrorMessage);
            if (Convert.ToInt64(PropertyValue.ToString()) > 0) return ValidationResult.Success;

            return new ValidationResult(ErrorMessage);
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            MergeAttribute(context.Attributes, "data-val", "true");
            var errorMessage = FormatErrorMessage(context.ModelMetadata.GetDisplayName());
            MergeAttribute(context.Attributes, "data-val-requiredif", errorMessage);
            MergeAttribute(context.Attributes, "data-val-other", "#" + PropertyName);
        }

        private bool MergeAttribute(IDictionary<string, string> attributes, string key, string value)
        {
            if (attributes.ContainsKey(key))
            {
                return false;
            }

            attributes.Add(key, value);
            return true;
        }
    }
}