﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;

namespace VW.MSU.UI.Middleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class JWTValidatorMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly JWTValidatorMiddlewareOptions _jwtValidatorMiddlewareOptions;

        public JWTValidatorMiddleware(RequestDelegate next, IOptions<JWTValidatorMiddlewareOptions> jwtValidatorMiddlewareOptions)
        {
            _next = next;
            _jwtValidatorMiddlewareOptions = jwtValidatorMiddlewareOptions.Value;
        }

        public Task Invoke(HttpContext httpContext)
        {
            try
            {
                if (httpContext.Request.Path.Value.IndexOf("/api") == 0)
                {
#if !DEBUG
                    var token = httpContext.Request.Headers["Authorization"][0].Replace("Bearer ", "").Trim();
                    if (ValidateToken(token))
                    {
                        return _next(httpContext);
                    }
#else
                    return _next(httpContext);
#endif
                }
                else
                    return _next(httpContext);
            }
            catch (Exception)
            {

            }

            return httpContext.Response.WriteAsync("Unable to process your request");
        }

        public ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
                JwtSecurityToken jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token);
                if (jwtToken == null)
                    return null;

                byte[] key = Convert.FromBase64String(_jwtValidatorMiddlewareOptions.JWTSecretKey);
                TokenValidationParameters parameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidIssuer = _jwtValidatorMiddlewareOptions.JWTIssuer,
                    ClockSkew = TimeSpan.Zero
                };

                ClaimsPrincipal principal = tokenHandler.ValidateToken(token,
                      parameters, out SecurityToken securityToken);

                return principal;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool ValidateToken(string token)
        {
            ClaimsPrincipal principal = GetPrincipal(token);
            if (principal == null)
                return false;
            try
            {
                ClaimsIdentity identity = (ClaimsIdentity)principal.Identity;
                return true;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class JWTValidatorMiddlewareExtensions
    {
        public static IApplicationBuilder UseJWTValidatorMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<JWTValidatorMiddleware>();
        }
    }

    public class JWTValidatorMiddlewareOptions
    {
        public string JWTIssuer { get; set; }
        public string JWTSecretKey { get; set; }
    }
}
