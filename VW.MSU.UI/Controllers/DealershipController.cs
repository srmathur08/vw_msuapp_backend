﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VW.MSU.Common;
using VW.MSU.Common.Enums;
using VW.MSU.Data.Models;
using VW.MSU.Models;
using VW.MSU.Services;
using VW.MSU.UI.Models;

namespace VW.MSU.UI.Controllers
{
    [Authorize]
    [SessionExpire]
    public class DealershipController : BaseController
    {
        public readonly ILogger<DealershipController> _logger;

        public DealershipController(IWebHostEnvironment hostingEnvironment, ILogger<DealershipController> logger, IConfiguration config, VWContext vwContext, IOptions<SFDCSettings> sfdcSettings)
            : base(hostingEnvironment, config, vwContext, sfdcSettings.Value)
        {
            _logger = logger;
        }

        #region Methods

        #endregion

        #region Create or Update Dealership

        public IActionResult CreateOrUpdate(long id = 0)
        {
            UserDetails userDetails = GetUserDetails();

            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)))
            {
                DealershipModel model;

                if (id > 0)
                {
                    var dealer = _vwContext.Dealership.Where(x => x.DealershipId == id).FirstOrDefault();
                    if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                    {
                        if (dealer.GroupDealerId != userDetails.GroupDealerId)
                            return RedirectToAction("Index", "Home");
                    }

                    model = new DealershipModel
                    {
                        Address = dealer.Address,
                        AdminEmail = dealer.AdminEmail,
                        AdminFullName = dealer.AdminFullName,
                        AdminMobile = dealer.AdminMobile,
                        Code = dealer.Code,
                        DealershipId = dealer.DealershipId,
                        DealershipType = dealer.DealershipType,
                        Landline = dealer.Landline,
                        Location = dealer.Location,
                        Mobile = dealer.Mobile,
                        Stdcode = dealer.Stdcode,
                        GroupDealerId = dealer.GroupDealerId.HasValue ? dealer.GroupDealerId.Value : 0,
                        AreaManagerId = dealer.AreaManagerId.HasValue ? dealer.AreaManagerId.Value : 0,
                        Name = dealer.Name,
                        Image = dealer.Image,
                        AreaManagers = _vwContext.Users.Where(x => !x.IsDeleted && x.IsActive && x.RoleId == (int)RoleTypes.AREAMANAGER).Select(z => new SelectListItem { Text = z.Fullname, Value = z.UserId.ToString(), Selected = dealer.AreaManagerId.HasValue && dealer.AreaManagerId.Value == z.UserId }).ToList(),
                        GroupDealers = _vwContext.GroupDealer.Where(z => !z.IsDeleted && z.IsActive).Select(z => new SelectListItem { Text = z.Name, Value = z.GroupDealerId.ToString(), Selected = dealer.GroupDealerId == z.GroupDealerId }).ToList()
                    };
                }
                else
                {
                    model = new DealershipModel
                    {
                        AreaManagers = _vwContext.Users.Where(x => !x.IsDeleted && x.IsActive && x.RoleId == (int)RoleTypes.AREAMANAGER).Select(z => new SelectListItem { Text = z.Fullname, Value = z.UserId.ToString() }).ToList(),
                        GroupDealers = _vwContext.GroupDealer.Where(z => !z.IsDeleted && z.IsActive).Select(z => new SelectListItem { Text = z.Name, Value = z.GroupDealerId.ToString() }).ToList()
                    };
                }

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public IActionResult CreateOrUpdate(DealershipModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    UserDetails userDetails = GetUserDetails();

                    var userId = int.Parse(HttpContext.User.Identity.Name ?? "-1");
                    var now = DateTime.Now;

                    Dealership dealer;
                    if (model.DealershipId > 0)
                    {
                        dealer = _vwContext.Dealership.Where(x => x.DealershipId == model.DealershipId).FirstOrDefault();
                    }
                    else
                    {
                        dealer = new Dealership
                        {
                            CreatedBy = userId,
                            CreatedDate = now,
                            IsDeleted = false,
                            IsActive = true
                        };
                    }

                    dealer.Name = model.Name;
                    dealer.Address = model.Address;
                    dealer.AdminEmail = model.AdminEmail;
                    dealer.AdminFullName = model.AdminFullName;
                    dealer.AdminMobile = model.AdminMobile;
                    dealer.Code = model.Code;
                    dealer.DealershipId = model.DealershipId;
                    dealer.DealershipType = model.DealershipType;
                    dealer.Landline = model.Landline;
                    dealer.Location = model.Location;
                    dealer.Mobile = model.Mobile;
                    dealer.Stdcode = model.Stdcode;

                    if (userDetails.CMSRoleId == (int)CMSRoleTypes.GROUPDEALERADMIN && userDetails.GroupDealerId > 0)
                        dealer.GroupDealerId = userDetails.GroupDealerId;
                    if (model.GroupDealerId > 0)
                        dealer.GroupDealerId = model.GroupDealerId;
                    else if (dealer.GroupDealerId > 0)
                        dealer.GroupDealerId = null;

                    if (model.AreaManagerId > 0)
                        dealer.AreaManagerId = model.AreaManagerId;

                    if (model.DealershipId > 0)
                        _vwContext.Dealership.Update(dealer);
                    else
                        _vwContext.Dealership.Add(dealer);

                    _vwContext.SaveChanges();

                    if (model.ImageBlob != null && model.ImageBlob.Length > 0)
                    {
                        var assetPath = $"{_hostingEnvironment.WebRootPath}/Content/Dealership/{dealer.DealershipId}";
                        var uploadFile = ConvertUploadFileToBase64String(model.ImageBlob);
                        UtilityServices.UploadBase64String(assetPath, $"{dealer.DealershipId}{Path.GetExtension(model.ImageBlob.FileName)}", uploadFile);
                        dealer.Image = $"/Content/Dealership/{dealer.DealershipId}/{dealer.DealershipId}{Path.GetExtension(model.ImageBlob.FileName)}";
                    }

                    _vwContext.Dealership.Update(dealer);
                    _vwContext.SaveChanges();

                    return RedirectToAction("List");
                }
                catch (Exception ex)
                {
                    model.ErrorMessage = ex.Message;
                }
            }

            model.AreaManagers = _vwContext.Users.Where(x => !x.IsDeleted && x.IsActive && x.RoleId == (int)RoleTypes.AREAMANAGER).Select(z => new SelectListItem { Text = z.Fullname, Value = z.UserId.ToString(), Selected = model.AreaManagerId == z.UserId }).ToList();
            model.GroupDealers = _vwContext.GroupDealer.Where(z => !z.IsDeleted && z.IsActive).Select(z => new SelectListItem { Text = z.Name, Value = z.GroupDealerId.ToString(), Selected = model.GroupDealerId == z.GroupDealerId }).ToList();

            return View(model);
        }

        #endregion

        public IActionResult List()
        {
            UserDetails userDetails = GetUserDetails();
            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)))
            {
                List<DealershipModel> model;
                if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                {
                    model = _vwContext.Dealership.Where(x => x.GroupDealerId == userDetails.GroupDealerId && !x.IsDeleted).OrderByDescending(x => x.CreatedDate).Select(x => new DealershipModel()
                    {
                        DealershipId = x.DealershipId,
                        DealershipType = x.DealershipType,
                        Landline = x.Landline,
                        Mobile = x.Mobile,
                        AdminFullName = x.AdminFullName,
                        AdminEmail = x.AdminEmail,
                        AdminMobile = x.AdminMobile,
                        IsActive = x.IsActive,
                        Location = x.Location,
                        Code = x.Code,
                        Name = x.Name
                    }).ToList();
                }
                else
                {
                    model = _vwContext.Dealership.Where(x => !x.IsDeleted).OrderByDescending(x => x.CreatedDate).Select(x => new DealershipModel()
                    {
                        DealershipId = x.DealershipId,
                        DealershipType = x.DealershipType,
                        Landline = x.Landline,
                        Mobile = x.Mobile,
                        AdminFullName = x.AdminFullName,
                        AdminEmail = x.AdminEmail,
                        AdminMobile = x.AdminMobile,
                        IsActive = x.IsActive,
                        Code = x.Code,
                        Name = x.Name
                    }).ToList();
                }

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public IActionResult Delete(long id)
        {
            UserDetails userDetails = GetUserDetails();
            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
            (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)))
            {
                var dealer = _vwContext.Dealership.Where(x => x.DealershipId == id).FirstOrDefault();
                dealer.IsDeleted = true;
                _vwContext.Dealership.Update(dealer);
                _vwContext.SaveChanges();

                return RedirectToAction("List");
            }
            else
                return RedirectToAction("Index", "Home");
        }
    }
}
