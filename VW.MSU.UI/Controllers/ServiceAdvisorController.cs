﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using VW.MSU.Common;
using VW.MSU.Common.Enums;
using VW.MSU.Data.Models;
using VW.MSU.Models;
using VW.MSU.UI.Models;

namespace VW.MSU.UI.Controllers
{
    public class ServiceAdvisorController : BaseController
    {
        public readonly ILogger<ServiceAdvisorController> _logger;
        public readonly int PAGE_SIZE = 4;

        public ServiceAdvisorController(IWebHostEnvironment hostingEnvironment, ILogger<ServiceAdvisorController> logger, IConfiguration config, VWContext vwContext, IOptions<SFDCSettings> sfdcSettings)
            : base(hostingEnvironment, config, vwContext, sfdcSettings.Value)
        {
            _logger = logger;
        }

        #region Methods

        private static ServiceRequestModel GetServiceRequestModel(ServiceRequest serviceRequest)
        {
            return new ServiceRequestModel()
            {
                Srid = serviceRequest.Srid,
                CustomerName = serviceRequest.CustomerName,
                DealershipId = serviceRequest.DealershipId,
                Duration = serviceRequest.Duration,
                InvoiceAmount = serviceRequest.InvoiceAmount,
                Srnumber = serviceRequest.Srnumber,
                ServiceDateTime = serviceRequest.ServiceDateTime,
                VehicleModel = serviceRequest.VehicleModel,
                Vinnumber = serviceRequest.Vinnumber,
                Lat = serviceRequest.Lat,
                Lng = serviceRequest.Lng,

            };
        }

        #endregion
        // GET: ServiceAdvisorController
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            UserDetails userDetails = GetUserDetails();
            List<UserModel> model;
            model = _vwContext.Users.OrderByDescending(x => x.CreatedDate).Select(x => new UserModel()
            {
                Fullname = x.Fullname,
                Email = x.Email,
                UserId = x.UserId,
                Lat = x.Lat,
                Lng = x.Lng,
                IsActive = x.IsActive
            }).ToList();

            return View(model);
        }

        public ActionResult UserServiceRequests(int id)
        {
            DateTime startDate = DateTime.Now.Date;
            DateTime endDate = startDate.AddDays(10);

            UserDetails userDetails = GetUserDetails();
            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)))
            {
                ServiceRequestListModel model = LoadSRs(1, userDetails, startDate, endDate, id);
                ViewBag.GoogleMapAPIKey = GoogleMapAPIKey;
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ServiceRequestListModel LoadSRs(int pageIndex, UserDetails userDetails, DateTime startDate, DateTime endDate, int assignee)
        {
            ServiceRequestListModel model = new ServiceRequestListModel();

            model.TotalSRs = _vwContext.ServiceRequest.Where(x => !x.IsDeleted && x.Assignee == assignee).Count();
            model.ServiceRequests = _vwContext.ServiceRequest.Where(x => !x.IsDeleted && x.Assignee == assignee)
                .OrderBy(x => x.ServiceDateTime)
                .Skip((pageIndex - 1) * PAGE_SIZE)
                .Take(PAGE_SIZE)
                .Select(x => GetServiceRequestModel(x)).ToList();

            model.AssigneeDetails = _vwContext.Users.Where(x => x.UserId == assignee && !x.IsDeleted).Select(x => new UserModel
            {
                UserId = x.UserId,
                Lat = x.Lat,
                Lng = x.Lng,
                Fullname=x.Fullname,
                Email = x.Email                

            }).FirstOrDefault();
            model.ServiceDate = startDate;
            return model;
        }

    }
}
