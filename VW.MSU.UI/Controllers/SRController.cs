﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using VW.MSU.Common;
using VW.MSU.Common.Enums;
using VW.MSU.Data.Models;
using VW.MSU.Models;
using VW.MSU.UI.Models;
using OfficeOpenXml;
using Microsoft.AspNetCore.Http;

namespace VW.MSU.UI.Controllers
{
    [Authorize]
    [SessionExpire]
    public class SRController : BaseController
    {
        public readonly ILogger<SRController> _logger;
        public readonly int PAGE_SIZE = 4;

        public SRController(IWebHostEnvironment hostingEnvironment, ILogger<SRController> logger, IConfiguration config, VWContext vwContext, IOptions<SFDCSettings> sfdcSettings)
            : base(hostingEnvironment, config, vwContext, sfdcSettings.Value)
        {
            _logger = logger;
        }

        #region Methods

        private static ServiceRequestModel GetServiceRequestModel(ServiceRequest serviceRequest)
        {
            return new ServiceRequestModel()
            {
                Srid = serviceRequest.Srid,
                CustomerName = serviceRequest.CustomerName,
                DealershipId = serviceRequest.DealershipId,
                Duration = serviceRequest.Duration,
                InvoiceAmount = serviceRequest.InvoiceAmount,
                Srnumber = serviceRequest.Srnumber,
                ServiceDateTime = serviceRequest.ServiceDateTime,
                VehicleModel = serviceRequest.VehicleModel,
                Vinnumber = serviceRequest.Vinnumber
            };
        }

        #endregion

        #region Create or Update User

        public IActionResult CreateOrUpdate(long id = 0)
        {
            UserDetails userDetails = GetUserDetails();

            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)))
            {
                ServiceRequestModel model;

                if (id > 0)
                {
                    var serviceRequest = _vwContext.ServiceRequest.Where(x => x.Srid == id).FirstOrDefault();
                    if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                    {
                        var dealerships = _vwContext.Dealership.Where(x => x.GroupDealerId == userDetails.GroupDealerId && !x.IsDeleted).Select(x => x.DealershipId).ToList();
                        if (!dealerships.Contains(serviceRequest.DealershipId))
                            return RedirectToAction("Index", "Home");
                    }
                    else if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
                    {
                        if (serviceRequest.DealershipId != userDetails.DealershipId)
                            return RedirectToAction("Index", "Home");
                    }

                    model = new ServiceRequestModel
                    {
                        Srid = serviceRequest.Srid,
                        Srnumber = serviceRequest.Srnumber,
                        Vinnumber = serviceRequest.Vinnumber,
                        CustomerName = serviceRequest.CustomerName,
                        VehicleModelId = serviceRequest.VehicleModelId ?? 0,
                        //VehicleModel = serviceRequest.VehicleModel,
                        VariantName = serviceRequest.VariantName,
                        InvoiceAmount = serviceRequest.InvoiceAmount,
                        Duration = serviceRequest.Duration,
                        IsOilChange = serviceRequest.IsOilChange,
                        IsBrakes = serviceRequest.IsBrakes,
                        IsTyreCheck = serviceRequest.IsTyreCheck,
                        IsBatteryCheck = serviceRequest.IsBatteryCheck,
                        ServiceDate = serviceRequest.ServiceDateTime.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture),
                        DealershipId = serviceRequest.DealershipId,
                        CreatedDate = serviceRequest.CreatedDate,
                        RegNum = serviceRequest.RegNum,
                        Lat = serviceRequest.Lat,
                        Lng = serviceRequest.Lng,
                        OdometerReading = serviceRequest.OdometerReading,
                        ServiceType = serviceRequest.ServiceType
                    };

                    model.Models = _vwContext.Models.Where(x => !x.IsDeleted).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = serviceRequest.VehicleModelId.HasValue ? serviceRequest.VehicleModelId.Value == x.Id : false }).ToList();

                    if (serviceRequest.Dealership.GroupDealerId.HasValue)
                        model.GroupDealerId = serviceRequest.Dealership.GroupDealerId.Value;

                    if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                    {
                        model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && x.GroupDealerId == userDetails.GroupDealerId).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = serviceRequest.DealershipId == x.DealershipId }).ToList();
                    }
                    else if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
                    {
                        
                    }
                    else
                    {
                        if (model.GroupDealerId > 0)
                            model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && x.GroupDealerId == model.GroupDealerId).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = serviceRequest.DealershipId == x.DealershipId }).ToList();
                        else
                            model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && !x.GroupDealerId.HasValue).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = serviceRequest.DealershipId == x.DealershipId }).ToList();
                    }
                    model.ServiceAdvisers = _vwContext.Users.Where(x=>!x.IsDeleted).Select(x=> new SelectListItem { Text = x.Fullname, Value = x.UserId.ToString(), Selected = serviceRequest.Assignee.HasValue ? serviceRequest.Assignee.Value == x.UserId : false }).ToList();
                }
                else
                {
                    model = new ServiceRequestModel() { GroupDealerId = userDetails.GroupDealerId, DealershipId = userDetails.DealershipId };

                    model.Models = _vwContext.Models.Where(x => !x.IsDeleted).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();

                    if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                    {
                        model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && x.GroupDealerId == userDetails.GroupDealerId).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString() }).ToList();
                    }
                    else if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
                    {
                    }
                    else
                    {
                        model.GroupDealers = userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN ? model.GroupDealers : _vwContext.GroupDealer.Where(z => !z.IsDeleted && z.IsActive).Select(z => new SelectListItem { Text = z.Name, Value = z.GroupDealerId.ToString() }).ToList();
                        model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && !x.GroupDealerId.HasValue).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString() }).ToList();
                    }
                }

                ViewBag.GoogleMapAPIKey = GoogleMapAPIKey;
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public IActionResult CreateOrUpdate(ServiceRequestModel model)
        {
            UserDetails userDetails = GetUserDetails();

            try
            {
                var userId = int.Parse(HttpContext.User.Identity.Name ?? "-1");
                var now = DateTime.Now;

                ServiceRequest serviceRequest;
                if (model.Srid > 0)
                {
                    serviceRequest = _vwContext.ServiceRequest.Where(x => x.Srid == model.Srid).FirstOrDefault();
                }
                else
                {
                    serviceRequest = new ServiceRequest
                    {
                        CreatedDate = now,
                        IsDeleted = false,
                        IsCompleted = false,
                        IsStarted = false
                    };
                }

                serviceRequest.Srnumber = model.Srnumber;
                serviceRequest.Vinnumber = model.Vinnumber;
                serviceRequest.CustomerName = model.CustomerName;
                //serviceRequest.VehicleModel = model.VehicleModel;
                serviceRequest.VariantName = model.VariantName;
                serviceRequest.InvoiceAmount = model.InvoiceAmount;
                serviceRequest.Duration = model.Duration;
                serviceRequest.IsOilChange = model.IsOilChange;
                serviceRequest.IsBrakes = model.IsBrakes;
                serviceRequest.IsTyreCheck = model.IsTyreCheck;
                serviceRequest.IsBatteryCheck = model.IsBatteryCheck;
                serviceRequest.ServiceDateTime = model.ServiceDateTime;
                serviceRequest.OdometerReading = model.OdometerReading;
                serviceRequest.ServiceType = model.ServiceType;
                serviceRequest.VehicleModelId = model.VehicleModelId;
                serviceRequest.Assignee = model.Assignee;

                if (!string.IsNullOrEmpty(model.Lat))
                    serviceRequest.Lat = model.Lat;

                if (!string.IsNullOrEmpty(model.Lng))
                    serviceRequest.Lng = model.Lng;

                if (!string.IsNullOrEmpty(model.RegNum))
                    serviceRequest.RegNum = model.RegNum;

                if (model.DealershipId > 0)
                    serviceRequest.DealershipId = model.DealershipId;
                
                if (model.Srid > 0)
                    _vwContext.ServiceRequest.Update(serviceRequest);
                else
                    _vwContext.ServiceRequest.Add(serviceRequest);

                _vwContext.SaveChanges();

                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                model.ErrorMessage = ex.Message;
            }

            model.Models = _vwContext.Models.Where(x => !x.IsDeleted).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = model.VehicleModelId == x.Id }).ToList();

            if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
            {
                model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && x.GroupDealerId == userDetails.GroupDealerId).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = model.DealershipId == x.DealershipId }).ToList();
            }
            else if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
            {
            }
            else
            {
                if (model.GroupDealerId > 0)
                    model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && x.GroupDealerId == model.GroupDealerId).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = model.DealershipId == x.DealershipId }).ToList();
                else
                    model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && !x.GroupDealerId.HasValue).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = model.DealershipId == x.DealershipId }).ToList();
                model.GroupDealers = _vwContext.GroupDealer.Where(z => !z.IsDeleted && z.IsActive).Select(z => new SelectListItem { Text = z.Name, Value = z.GroupDealerId.ToString(), Selected = model.GroupDealerId == z.GroupDealerId }).ToList();
            }

            ViewBag.GoogleMapAPIKey = GoogleMapAPIKey;
            return View(model);
        }

        #endregion

        public IActionResult List()
        {
            DateTime startDate = DateTime.Now.Date;
            DateTime endDate = startDate.AddDays(1);

            UserDetails userDetails = GetUserDetails();
            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)))
            {
                ServiceRequestListModel model = LoadSRs(1, userDetails, startDate, endDate);
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public IActionResult List(string serviceDate)
        {
            DateTime startDate;
            if (DateTime.TryParseExact(serviceDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate))
            {
                DateTime endDate = startDate.AddDays(1);

                UserDetails userDetails = GetUserDetails();
                if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                    (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)))
                {
                    ServiceRequestListModel model = LoadSRs(1, userDetails, startDate, endDate);
                    return View(model);
                }
            }

            return RedirectToAction("List", "SR");
        }

        [HttpPost]
        public IActionResult FetchNextSRs(int pageIndex, string serviceDate)
        {
            DateTime startDate;
            if (DateTime.TryParseExact(serviceDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate))
            {
                DateTime endDate = startDate.AddDays(1);

                UserDetails userDetails = GetUserDetails();
                if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                    (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)))
                {
                    ServiceRequestListModel model = LoadSRs(pageIndex, userDetails, startDate, endDate);
                    return PartialView("_SRRenderRows", model.ServiceRequests);
                }
            }

            return new ContentResult();
        }

        public ServiceRequestListModel LoadSRs(int pageIndex, UserDetails userDetails, DateTime startDate, DateTime endDate)
        {
            ServiceRequestListModel model = new ServiceRequestListModel();
            if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
            {
                var dealerships = _vwContext.Dealership.Where(x => x.GroupDealerId == userDetails.GroupDealerId && !x.IsDeleted).Select(x => x.DealershipId).ToList();
                model.TotalSRs = _vwContext.ServiceRequest.Where(x => dealerships.Contains(x.DealershipId) && !x.IsDeleted && x.ServiceDateTime >= startDate && x.ServiceDateTime < endDate).Count();
                model.ServiceRequests = _vwContext.ServiceRequest.Where(x => dealerships.Contains(x.DealershipId) && !x.IsDeleted && x.ServiceDateTime >= startDate && x.ServiceDateTime < endDate)
                    .OrderByDescending(x => x.CreatedDate)
                    .Skip((pageIndex - 1) * PAGE_SIZE)
                    .Take(PAGE_SIZE)
                    .Select(x => GetServiceRequestModel(x)).ToList();
            }
            else if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
            {
                model.TotalSRs = _vwContext.ServiceRequest.Where(x => x.DealershipId == userDetails.DealershipId && !x.IsDeleted && x.ServiceDateTime >= startDate && x.ServiceDateTime < endDate).Count();
                model.ServiceRequests = _vwContext.ServiceRequest.Where(x => x.DealershipId == userDetails.DealershipId && !x.IsDeleted && x.ServiceDateTime >= startDate && x.ServiceDateTime < endDate)
                    .OrderByDescending(x => x.CreatedDate)
                    .Skip((pageIndex - 1) * PAGE_SIZE)
                    .Take(PAGE_SIZE)
                    .Select(x => GetServiceRequestModel(x)).ToList();
            }
            else
            {
                model.TotalSRs = _vwContext.ServiceRequest.Where(x => !x.IsDeleted && x.ServiceDateTime >= startDate && x.ServiceDateTime < endDate).Count();
                model.ServiceRequests = _vwContext.ServiceRequest.Where(x => !x.IsDeleted && x.ServiceDateTime >= startDate && x.ServiceDateTime < endDate)
                    .OrderByDescending(x => x.CreatedDate)
                    .Skip((pageIndex - 1) * PAGE_SIZE)
                    .Take(PAGE_SIZE)
                    .Select(x => GetServiceRequestModel(x)).ToList();
            }

            model.ServiceDate = startDate;
            return model;
        }

        public IActionResult Delete(long id)
        {
            UserDetails userDetails = GetUserDetails();
            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
            (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)))
            {
                ServiceRequest serviceRequest;
                if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                {
                    var dealerships = _vwContext.Dealership.Where(x => x.GroupDealerId == userDetails.GroupDealerId && !x.IsDeleted).Select(x => x.DealershipId).ToList();
                    serviceRequest = _vwContext.ServiceRequest.Where(x => x.Srid == id && dealerships.Contains(x.DealershipId)).FirstOrDefault();
                }
                else if (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
                {
                    serviceRequest = _vwContext.ServiceRequest.Where(x => x.Srid == id && x.DealershipId == userDetails.DealershipId).FirstOrDefault();
                }
                else
                {
                    serviceRequest = _vwContext.ServiceRequest.Where(x => x.Srid == id).FirstOrDefault();
                }

                if (serviceRequest != null)
                {
                    serviceRequest.IsDeleted = true;
                    _vwContext.ServiceRequest.Update(serviceRequest);
                    _vwContext.SaveChanges();
                }

                return RedirectToAction("List");
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public IActionResult ExportToExcel(string serviceDate)
        {
            DateTime startDate;
            if (DateTime.TryParseExact(serviceDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate))
            {
                DateTime endDate = startDate.AddDays(1);
                List<ServiceRequestModel> report = new List<ServiceRequestModel>();

                    UserDetails userDetails = GetUserDetails();
                if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                    (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)))
                {
                    if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                    {
                        var dealerships = _vwContext.Dealership.Where(x => x.GroupDealerId == userDetails.GroupDealerId && !x.IsDeleted).Select(x => x.DealershipId).ToList();
                        report = _vwContext.ServiceRequest.Where(x => dealerships.Contains(x.DealershipId) && !x.IsDeleted && x.ServiceDateTime >= startDate && x.ServiceDateTime < endDate)
                            .OrderByDescending(x => x.CreatedDate)
                            .Select(x => GetServiceRequestModel(x)).ToList();
                    }
                    else if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
                    {
                        report = _vwContext.ServiceRequest.Where(x => x.DealershipId == userDetails.DealershipId && !x.IsDeleted && x.ServiceDateTime >= startDate && x.ServiceDateTime < endDate)
                            .OrderByDescending(x => x.CreatedDate)
                            .Select(x => GetServiceRequestModel(x)).ToList();
                    }
                    else
                    {
                        report = _vwContext.ServiceRequest.Where(x => !x.IsDeleted && x.ServiceDateTime >= startDate && x.ServiceDateTime < endDate)
                            .OrderByDescending(x => x.CreatedDate)
                            .Select(x => GetServiceRequestModel(x)).ToList();
                    }

                    byte[] fileContents;

                    ExcelPackage Ep = new ExcelPackage();
                    ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Service Request");
                    Sheet.Cells["A1"].Value = "SR Number";
                    Sheet.Cells["B1"].Value = "Customer Name";
                    Sheet.Cells["C1"].Value = "VIN Number";
                    Sheet.Cells["D1"].Value = "Service Date & Time";

                    int row = 2;
                    foreach (var item in report)
                    {
                        Sheet.Cells[string.Format("A{0}", row)].Value = item.Srnumber;
                        Sheet.Cells[string.Format("B{0}", row)].Value = item.CustomerName;
                        Sheet.Cells[string.Format("C{0}", row)].Value = item.Vinnumber;
                        Sheet.Cells[string.Format("D{0}", row)].Value = item.ServiceDateTime.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);
                        row++;
                    }


                    Sheet.Cells["A:AZ"].AutoFitColumns();
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    fileContents = Ep.GetAsByteArray();

                    if (fileContents == null || fileContents.Length == 0)
                    {
                        return NotFound();
                    }

                    return File(
                        fileContents: fileContents,
                        contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        fileDownloadName: "Service Requests.xlsx"
                    );
                }
            }

            return new ContentResult();
        }
    }
}
