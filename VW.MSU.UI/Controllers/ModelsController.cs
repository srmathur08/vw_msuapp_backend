﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VW.MSU.Common;
using VW.MSU.Data.Models;
using VW.MSU.Models;
using VW.MSU.Services;
using VW.MSU.UI.Models;

namespace VW.MSU.UI.Controllers
{
    [Authorize]
    [SessionExpire]
    public class ModelsController : BaseController
    {
        public readonly ILogger<ModelsController> _logger;

        public ModelsController(IWebHostEnvironment hostingEnvironment, ILogger<ModelsController> logger, IConfiguration config, VWContext vwContext, IOptions<SFDCSettings> sfdcSettings)
            : base(hostingEnvironment, config, vwContext, sfdcSettings.Value)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            List<ModelsModel> model;

            model = _vwContext.Models.Where(x => !x.IsDeleted).Select(x => new ModelsModel
            {
                Id = x.Id,
                Name = x.Name
            }).OrderBy(x => x.Name).ToList();

            return View(model);
        }

        public IActionResult CreateOrUpdate(int id)
        {
            ModelsModel modelsModel = new ModelsModel();
            Data.Models.Models model = null;

            if (id > 0)
                model = _vwContext.Models.Where(x => x.Id == id).FirstOrDefault();

            if (model != null)
            {
                modelsModel = new ModelsModel()
                {
                    Id = model.Id,
                    Name = model.Name,
                    Image = model.Image,
                    FrontAngleImage = model.FrontAngleImage,
                    BackAngleImage = model.BackAngleImage,
                    RightAngleImage = model.RightAngleImage,
                    LeftAngleImage = model.LeftAngleImage
                };
            }

            return View(modelsModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateOrUpdate(ModelsModel modelsModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Data.Models.Models model = null;
                    if (modelsModel.Id > 0)
                    {
                        model = _vwContext.Models.Where(x => x.Id == modelsModel.Id).FirstOrDefault();
                    }
                    else
                    {
                        model = new Data.Models.Models
                        {
                            IsDeleted = false
                        };
                    }

                    model.Name = modelsModel.Name;

                    if (modelsModel.Id > 0)
                    {
                        _vwContext.Models.Update(model);
                    }
                    else
                    {
                        _vwContext.Models.Add(model);
                    }

                    _vwContext.SaveChanges();

                    if (modelsModel.ImageBlob != null && modelsModel.ImageBlob.Length > 0)
                    {
                        var assetPath = $"{_hostingEnvironment.WebRootPath}/Content/Models/{model.Id}";
                        var uploadFile = string.Empty;

                        using (var ms = new MemoryStream())
                        {
                            modelsModel.ImageBlob.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            uploadFile = Convert.ToBase64String(fileBytes);
                        }

                        UtilityServices.UploadBase64String(assetPath, $"{model.Id}{Path.GetExtension(modelsModel.ImageBlob.FileName)}", uploadFile);
                        model.Image = $"/Content/Models/{model.Id}/{model.Id}{Path.GetExtension(modelsModel.ImageBlob.FileName)}";
                    }

                    if (modelsModel.FrontAngleImageBlob != null && modelsModel.FrontAngleImageBlob.Length > 0)
                    {
                        var assetPath = $"{_hostingEnvironment.WebRootPath}/Content/Models/{model.Id}";
                        var uploadFile = string.Empty;

                        using (var ms = new MemoryStream())
                        {
                            modelsModel.FrontAngleImageBlob.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            uploadFile = Convert.ToBase64String(fileBytes);
                        }

                        UtilityServices.UploadBase64String(assetPath, $"{model.Id}_Front{Path.GetExtension(modelsModel.FrontAngleImageBlob.FileName)}", uploadFile);
                        model.FrontAngleImage = $"/Content/Models/{model.Id}/{model.Id}_Front{Path.GetExtension(modelsModel.FrontAngleImageBlob.FileName)}";
                    }

                    if (modelsModel.BackAngleImageBlob != null && modelsModel.BackAngleImageBlob.Length > 0)
                    {
                        var assetPath = $"{_hostingEnvironment.WebRootPath}/Content/Models/{model.Id}";
                        var uploadFile = string.Empty;

                        using (var ms = new MemoryStream())
                        {
                            modelsModel.BackAngleImageBlob.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            uploadFile = Convert.ToBase64String(fileBytes);
                        }

                        UtilityServices.UploadBase64String(assetPath, $"{model.Id}_Back{Path.GetExtension(modelsModel.BackAngleImageBlob.FileName)}", uploadFile);
                        model.BackAngleImage = $"/Content/Models/{model.Id}/{model.Id}_Back{Path.GetExtension(modelsModel.BackAngleImageBlob.FileName)}";
                    }

                    if (modelsModel.LeftAngleImageBlob != null && modelsModel.LeftAngleImageBlob.Length > 0)
                    {
                        var assetPath = $"{_hostingEnvironment.WebRootPath}/Content/Models/{model.Id}";
                        var uploadFile = string.Empty;

                        using (var ms = new MemoryStream())
                        {
                            modelsModel.LeftAngleImageBlob.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            uploadFile = Convert.ToBase64String(fileBytes);
                        }

                        UtilityServices.UploadBase64String(assetPath, $"{model.Id}_Left{Path.GetExtension(modelsModel.LeftAngleImageBlob.FileName)}", uploadFile);
                        model.LeftAngleImage = $"/Content/Models/{model.Id}/{model.Id}_Left{Path.GetExtension(modelsModel.LeftAngleImageBlob.FileName)}";
                    }

                    if (modelsModel.RightAngleImageBlob != null && modelsModel.RightAngleImageBlob.Length > 0)
                    {
                        var assetPath = $"{_hostingEnvironment.WebRootPath}/Content/Models/{model.Id}";
                        var uploadFile = string.Empty;

                        using (var ms = new MemoryStream())
                        {
                            modelsModel.RightAngleImageBlob.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            uploadFile = Convert.ToBase64String(fileBytes);
                        }

                        UtilityServices.UploadBase64String(assetPath, $"{model.Id}_Right{Path.GetExtension(modelsModel.RightAngleImageBlob.FileName)}", uploadFile);
                        model.RightAngleImage = $"/Content/Models/{model.Id}/{model.Id}_Right{Path.GetExtension(modelsModel.RightAngleImageBlob.FileName)}";
                    }

                    _vwContext.Models.Update(model);
                    _vwContext.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    modelsModel.ErrorMessage = ex.Message;
                }
            }

            return View(modelsModel);
        }

        public IActionResult Delete(int id)
        {
            var model = _vwContext.Models.Where(x => x.Id == id).FirstOrDefault();

            if (model != null)
            {
                model.IsDeleted = true;
                _vwContext.Models.Update(model);
                _vwContext.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}