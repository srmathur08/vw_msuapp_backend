﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VW.MSU.Common;
using VW.MSU.Data.Models;
using VW.MSU.Models;
using VW.MSU.Services;
using VW.MSU.UI.Models;

namespace VW.MSU.UI.Controllers.Api
{
    [ApiController]
    [Route("api/[controller]")]
    public class SRController : ApiBaseController
    {
        public readonly ILogger<SRController> _logger;

        public SRController(IWebHostEnvironment hostingEnvironment, ILogger<SRController> logger, IConfiguration config, VWContext vwContext)
            : base(hostingEnvironment, config, vwContext)
        {
            _logger = logger;
        }

        #region Methods

        public static ServiceRequestModel PrepareServiceRequestModel(ServiceRequest serviceRequest)
        {
            ServiceRequestModel serviceRequestModel = new ServiceRequestModel
            {
                Srid = serviceRequest.Srid,
                Srnumber = serviceRequest.Srnumber,
                Vinnumber = serviceRequest.Vinnumber,
                CustomerName = serviceRequest.CustomerName,
                VehicleModel = serviceRequest.VehicleModelId.HasValue ? serviceRequest.VehicleModelNavigation.Name : string.Empty,// x.VehicleModel,
                VariantName = serviceRequest.VariantName,
                InvoiceAmount = serviceRequest.InvoiceAmount,
                Duration = serviceRequest.Duration,
                IsOilChange = serviceRequest.IsOilChange,
                IsBrakes = serviceRequest.IsBrakes,
                IsTyreCheck = serviceRequest.IsTyreCheck,
                IsBatteryCheck = serviceRequest.IsBatteryCheck,
                ServiceDateTime = serviceRequest.ServiceDateTime,
                Lat = serviceRequest.Lat,
                Lng = serviceRequest.Lng,
                RegNum = serviceRequest.RegNum,
                OdometerReading = serviceRequest.OdometerReading,
                ServiceType = serviceRequest.ServiceType,
                VehicleImage = serviceRequest.VehicleModelId.HasValue ? serviceRequest.VehicleModelNavigation.Image : string.Empty,
                IsStarted = serviceRequest.IsStarted,
                IsCompleted = serviceRequest.IsCompleted
            };

            if (serviceRequest.VehicleCheckup.Count() > 0)
            {
                serviceRequestModel.IsVCCompleted = serviceRequest.VehicleCheckup.First().IsCompleted;
            }

            return serviceRequestModel;
        }

        #endregion

        #region APIs

        #region SR

        [HttpPost]
        [Route("SRsByDate")]
        public dynamic SRsByDate([FromBody] InputModel inputModel)
        {
            try
            {
                ServiceRequestModel model = JsonConvert.DeserializeObject<ServiceRequestModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));
                var endDate = model.ServiceDateTime.AddDays(1);

                var serviceRequests = _vwContext.ServiceRequest.Where(x => !x.IsDeleted && x.DealershipId == model.DealershipId && 
                    x.ServiceDateTime >= model.ServiceDateTime.Date && x.ServiceDateTime < endDate.Date).OrderBy(x => x.ServiceDateTime).Select(x => PrepareServiceRequestModel(x)).ToList();

                var output = JsonConvert.SerializeObject(new { Result = "success", ServiceRequests = serviceRequests });
                return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
            }
            catch (Exception) 
            { 
            }

            return new { Data = ApiFailureString };
        }

        [HttpPost]
        [Route("StartSR")]
        public dynamic StartSR([FromBody] InputModel inputModel)
        {
            try
            {
                ServiceRequestModel model = JsonConvert.DeserializeObject<ServiceRequestModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));
                var serviceRequest = _vwContext.ServiceRequest.Where(x => x.Srid == model.Srid).FirstOrDefault();

                serviceRequest.IsStarted = true;
                serviceRequest.StartTime = DateTime.Now;

                _vwContext.ServiceRequest.Update(serviceRequest);
                _vwContext.SaveChanges();

                var output = JsonConvert.SerializeObject(new { Result = "success" });
                return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
            }
            catch { }

            return new { Data = ApiFailureString };
        }

        [HttpPost]
        [Route("CompleteSR")]
        public dynamic CompleteSR([FromBody] InputModel inputModel)
        {
            try
            {
                ServiceRequestModel model = JsonConvert.DeserializeObject<ServiceRequestModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));
                var serviceRequest = _vwContext.ServiceRequest.Where(x => x.Srid == model.Srid).FirstOrDefault();

                serviceRequest.IsCompleted = true;
                serviceRequest.CompleteTime = DateTime.Now;

                _vwContext.ServiceRequest.Update(serviceRequest);
                _vwContext.SaveChanges();

                var output = JsonConvert.SerializeObject(new { Result = "success" });
                return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
            }
            catch { }

            return new { Data = ApiFailureString };
        }

        #endregion

        #region Vehicle Checkup

        [HttpPost]
        [Route("CreateVC")]
        public dynamic CreateVC([FromBody] InputModel inputModel)
        {
            try
            {
                VehicleCheckupModel model = JsonConvert.DeserializeObject<VehicleCheckupModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));

                VehicleCheckup vehicleCheckup = _vwContext.VehicleCheckup.Where(x => x.Srid == model.Srid).FirstOrDefault();
                if (vehicleCheckup != null)
                {
                    var output = JsonConvert.SerializeObject(new { Result = "failure", Reason = "ALREADY_EXISTS",  VehicleCheckupId = vehicleCheckup.Id });
                    return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
                }
                else
                {
                    vehicleCheckup = new VehicleCheckup()
                    {
                        Srid = model.Srid,
                        VehicleMileage = model.VehicleMileage,
                        FuelLevel = model.FuelLevel,
                        CreatedDate = DateTime.Now,
                        IsCompleted = false,
                    };

                    _vwContext.VehicleCheckup.Add(vehicleCheckup);
                    _vwContext.SaveChanges();

                    var output = JsonConvert.SerializeObject(new { Result = "success", VehicleCheckupId = vehicleCheckup.Id });
                    return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
                }
            }
            catch { }

            return new { Data = ApiFailureString };
        }

        [HttpPost]
        [Route("CreateVCAsset")]
        public dynamic CreateVCAsset([FromBody] InputModel inputModel)
        {
            try
            {
                VehicleCheckupAssetModel model = JsonConvert.DeserializeObject<VehicleCheckupAssetModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));

                if (!string.IsNullOrEmpty(model.AssetBase64String))
                {
                    VehicleCheckup vehicleCheckup = _vwContext.VehicleCheckup.Where(x => x.Id == model.VehicleCheckupId).FirstOrDefault();

                    VehicleCheckupAsset vehicleCheckupAsset = new VehicleCheckupAsset()
                    {
                        VehicleCheckupId = model.VehicleCheckupId,
                        CreatedDate = DateTime.Now
                    };

                    _vwContext.VehicleCheckupAsset.Add(vehicleCheckupAsset);
                    _vwContext.SaveChanges();

                    bool IsVideo = false;
                    var extension = model.Asset.Substring(model.Asset.LastIndexOf("."));
                    if (VideoExts.Contains(extension))
                    {
                        IsVideo = true;
                    }

                    var assetPath = $"{_hostingEnvironment.WebRootPath}/Content/SR/{vehicleCheckup.Srid}";
                    UtilityServices.UploadBase64String(assetPath, $"{vehicleCheckupAsset.Id}{Path.GetExtension(model.Asset)}", model.AssetBase64String);
                    vehicleCheckupAsset.Asset = $"/Content/SR/{vehicleCheckup.Srid}/{vehicleCheckupAsset.Id}{Path.GetExtension(model.Asset)}";
                    if (IsVideo)
                        vehicleCheckupAsset.Thumb = $"/Content/SR/{vehicleCheckup.Srid}/{vehicleCheckupAsset.Id}_Thumb.jpg";

                    if (IsVideo && model.IsLastChunk)
                    {
                        UtilityServices.GenerateAndSaveThumbForVideo(_hostingEnvironment.WebRootPath, $"{_hostingEnvironment.WebRootPath}{vehicleCheckupAsset.Asset}");
                    }

                    _vwContext.VehicleCheckupAsset.Update(vehicleCheckupAsset);
                    _vwContext.SaveChanges();

                    var output = JsonConvert.SerializeObject(new { Result = "success", AssetPath = string.IsNullOrEmpty(vehicleCheckupAsset.Asset) ? string.Empty : vehicleCheckupAsset.Asset });
                    return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
                }
            }
            catch { }

            return new { Data = ApiFailureString };
        }

        [HttpPost]
        [Route("CreateVCData")]
        public dynamic CreateVCData([FromBody] InputModel inputModel)
        {
            try
            {
                VehicleCheckupDataModel model = JsonConvert.DeserializeObject<VehicleCheckupDataModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));
                VehicleCheckup vehicleCheckup = _vwContext.VehicleCheckup.Where(x => x.Id == model.VehicleCheckupId).FirstOrDefault();

                VehicleCheckupData vehicleCheckupData = new VehicleCheckupData()
                {
                    Name = model.Name,
                    SectionName = model.SectionName,
                    VehicleCheckupId = model.VehicleCheckupId,
                    Status = model.Status,
                    CreatedDate = DateTime.Now
                };

                if (string.IsNullOrEmpty(model.Remarks))
                {
                    vehicleCheckupData.Remarks = model.Remarks;
                }

                if (string.IsNullOrEmpty(model.Value))
                {
                    vehicleCheckupData.Value = model.Value;
                }

                _vwContext.VehicleCheckupData.Add(vehicleCheckupData);
                _vwContext.SaveChanges();

                var output = JsonConvert.SerializeObject(new { Result = "success", VehicleCheckupDataId = vehicleCheckupData.Id });
                return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
            }
            catch { }

            return new { Data = ApiFailureString };
        }

        [HttpPost]
        [Route("CreateVCDataArray")]
        public dynamic CreateVCDataArray([FromBody] InputModel inputModel)
        {
            try
            {
                ReqVehicleCheckupDataArrayModel model = JsonConvert.DeserializeObject<ReqVehicleCheckupDataArrayModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));
                VehicleCheckup vehicleCheckup = _vwContext.VehicleCheckup.Where(x => x.Id == model.VehicleCheckupId).FirstOrDefault();

                List<ResVehicleCheckupDataModel> response = new List<ResVehicleCheckupDataModel>();

                foreach (var checkupData in model.CheckupData)
                {
                    VehicleCheckupData vehicleCheckupData = new VehicleCheckupData()
                    {
                        Name = checkupData.Name,
                        SectionName = checkupData.SectionName,
                        VehicleCheckupId = checkupData.VehicleCheckupId,
                        Status = checkupData.Status,
                        CreatedDate = DateTime.Now
                    };

                    if (!string.IsNullOrEmpty(checkupData.Remarks))
                    {
                        vehicleCheckupData.Remarks = checkupData.Remarks;
                    }

                    if (!string.IsNullOrEmpty(checkupData.Value))
                    {
                        vehicleCheckupData.Value = checkupData.Value;
                    }

                    _vwContext.VehicleCheckupData.Add(vehicleCheckupData);
                    _vwContext.SaveChanges();

                    response.Add(new ResVehicleCheckupDataModel { VehicleCheckupDataId = vehicleCheckupData.Id, SectionName = vehicleCheckupData.SectionName, Name = vehicleCheckupData.Name });
                }

                var output = JsonConvert.SerializeObject(new { Result = "success", CheckupData = response });
                return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
            }
            catch { }

            return new { Data = ApiFailureString };
        }

        [HttpPost]
        [Route("CreateVCDataAsset")]
        public dynamic CreateVCDataAsset([FromBody] InputModel inputModel)
        {
            try
            {
                VehicleCheckupDataAssetModel model = JsonConvert.DeserializeObject<VehicleCheckupDataAssetModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));
                VehicleCheckupData vehicleCheckupData = _vwContext.VehicleCheckupData.Where(x => x.Id == model.VehicleCheckupDataId).FirstOrDefault();

                VehicleCheckupDataAsset obj = new VehicleCheckupDataAsset()
                {
                    VehicleCheckupDataId = model.VehicleCheckupDataId
                };

                _vwContext.VehicleCheckupDataAsset.Add(obj);
                _vwContext.SaveChanges();

                if (!string.IsNullOrEmpty(model.AssetBase64String))
                {
                    bool IsVideo = false;
                    var extension = model.Asset.Substring(model.Asset.LastIndexOf("."));
                    if (VideoExts.Contains(extension))
                    {
                        IsVideo = true;
                    }

                    var assetPath = $"{_hostingEnvironment.WebRootPath}/Content/SR/{vehicleCheckupData.VehicleCheckup.Srid}/VehicleCheckup/{vehicleCheckupData.Id}";
                    UtilityServices.UploadBase64String(assetPath, $"{obj.Id}{Path.GetExtension(model.Asset)}", model.AssetBase64String);
                    obj.Asset = $"/Content/SR/{vehicleCheckupData.VehicleCheckup.Srid}/VehicleCheckup/{vehicleCheckupData.Id}/{obj.Id}{Path.GetExtension(model.Asset)}";
                    if (IsVideo)
                        obj.Thumb = $"/Content/SR/{vehicleCheckupData.VehicleCheckup.Srid}/VehicleCheckup/{vehicleCheckupData.Id}/{obj.Id}_Thumb.jpg";

                    if (IsVideo && model.IsLastChunk)
                    {
                        UtilityServices.GenerateAndSaveThumbForVideo(_hostingEnvironment.WebRootPath, $"{_hostingEnvironment.WebRootPath}{obj.Asset}");
                    }

                    _vwContext.VehicleCheckupDataAsset.Update(obj);
                    _vwContext.SaveChanges();
                }

                var output = JsonConvert.SerializeObject(new { Result = "success", VehicleCheckupDataAssetId = obj.Id, AssetPath = string.IsNullOrEmpty(obj.Asset) ? string.Empty : obj.Asset });
                return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
            }
            catch { }

            return new { Data = ApiFailureString };
        }

        [HttpPost]
        [Route("GetVCData")]
        public dynamic GetVCData([FromBody] InputModel inputModel)
        {
            try
            {
                VehicleCheckupModel model = JsonConvert.DeserializeObject<VehicleCheckupModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));
                var response = _vwContext.VehicleCheckup.Where(x => x.Srid == model.Srid).Select(x => new ResponseVehicleCheckupModel
                {
                    Id = x.Id,
                    Srid = x.Srid,
                    FuelLevel = x.FuelLevel.HasValue ? x.FuelLevel.Value : 0,
                    VehicleMileage = x.VehicleMileage,
                    IsCompleted = x.IsCompleted,
                    VehicleCheckupAssets = x.VehicleCheckupAsset.OrderBy(y => y.CreatedDate).Select(y => new VehicleCheckupAssetModel
                    {
                        Asset = y.Asset,
                        Id = y.Id,
                        Thumb = y.Thumb
                    }).ToList(),
                    VehicleCheckupData = x.VehicleCheckupData.OrderBy(y => y.SectionName).Select(y => new ResponseVehicleCheckupDataModel
                    {
                        Name = y.Name,
                        SectionName = y.SectionName,
                        Value = y.Value,
                        Id = y.Id,
                        Remarks = y.Remarks,
                        VehicleCheckupId = y.VehicleCheckupId,
                        Status = y.Status,
                        Assets = y.VehicleCheckupDataAsset.Select(z => new VehicleCheckupDataAssetModel
                        {
                            Asset = z.Asset,
                            Thumb = z.Thumb,
                            Id = z.Id,
                            VehicleCheckupDataId = z.VehicleCheckupDataId
                        }).ToList()
                    }).ToList()
                });

                var output = JsonConvert.SerializeObject(new { Result = "success", VehicleCheckup = response });
                return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
            }
            catch { }

            return new { Data = ApiFailureString };
        }



        [HttpPost]
        [Route("CompleteVC")]
        public dynamic CompleteVC([FromBody] InputModel inputModel)
        {
            try
            {
                VehicleCheckupModel model = JsonConvert.DeserializeObject<VehicleCheckupModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));
                var vehicleCheckup = _vwContext.VehicleCheckup.Where(x => x.Id == model.Id).FirstOrDefault();

                vehicleCheckup.IsCompleted = true;
                //vehicleCheckup.CompleteTime = DateTime.Now;

                _vwContext.VehicleCheckup.Update(vehicleCheckup);
                _vwContext.SaveChanges();

                var output = JsonConvert.SerializeObject(new { Result = "success" });
                return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
            }
            catch { }

            return new { Data = ApiFailureString };
        }        

        #endregion

        #region Append Asset

        [HttpPost]
        [Route("AppendAsset")]
        public dynamic AppendAsset([FromBody] InputModel inputModel)
        {
            try
            {
                AppendAssetModel model = JsonConvert.DeserializeObject<AppendAssetModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));
                var assetPath = model.AssetPath.Substring(0, model.AssetPath.LastIndexOf("/"));
                var assetName = model.AssetPath.Substring(model.AssetPath.LastIndexOf("/") + 1);
                UtilityServices.AppendBase64String($"{_hostingEnvironment.WebRootPath}{assetPath}", $"{assetName}", model.Asset);

                bool IsVideo = false;
                var extension = model.AssetPath.Substring(model.AssetPath.LastIndexOf("."));
                if (VideoExts.Contains(extension))
                {
                    IsVideo = true;
                }

                if (IsVideo && model.IsLastChunk)
                {
                    UtilityServices.GenerateAndSaveThumbForVideo(_hostingEnvironment.WebRootPath, $"{_hostingEnvironment.WebRootPath}{assetPath}/{assetName}");
                }

                var output = JsonConvert.SerializeObject(new { Result = "success" });
                return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
            }
            catch { }

            return new { Data = ApiFailureString };
        }

        #endregion

        #endregion
    }
}
