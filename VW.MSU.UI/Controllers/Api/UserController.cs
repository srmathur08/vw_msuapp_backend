﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using VW.MSU.Common;
using VW.MSU.Common.Enums;
using VW.MSU.Data.Models;
using VW.MSU.Models;
using VW.MSU.UI.Models;

namespace VW.MSU.UI.Controllers.Api
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ApiBaseController
    {
        #region Fields

        public readonly ILogger<UserController> _logger;

        #endregion

        #region Ctor

        public UserController(IWebHostEnvironment hostingEnvironment, ILogger<UserController> logger, IConfiguration config, VWContext vwContext)
            : base(hostingEnvironment, config, vwContext)
        {
            _logger = logger;
        }

        #endregion

        #region APIs

        [HttpPost]
        [Route("Login")]
        public dynamic Login([FromBody] InputModel inputModel)
        {
            try
            {
                LoginModel loginModel = JsonConvert.DeserializeObject<LoginModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));
                var password = EncryptDecryptUtils.Encrypt(PhraseKey, loginModel.Password);
                var user = _vwContext.Users.Where(x => !x.IsDeleted && x.IsActive && EF.Functions.Like(x.Phone, loginModel.PhoneNo) && EF.Functions.Like(x.Password, password)).FirstOrDefault();
                if (user != null)
                {
                    var output = JsonConvert.SerializeObject(new
                    {
                        Result = "success",
                        RoleId = user.RoleId ?? 0,
                        Role = user.RoleId.HasValue ? user.Role.Name : string.Empty,
                        DealershipId = user.DealershipId.HasValue ? user.DealershipId.Value : 0,
                        Dealership = user.DealershipId.HasValue ? user.DealershipNavigation.Name : string.Empty,
                        user.Fullname,
                        user.Email,
                        user.UserId
                    });
                    return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
                }
                else
                {
                    var output1 = JsonConvert.SerializeObject(new { Result = "failure", Reason = "Invalid Mobile/Password" });
                    return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output1) };
                }
            }
            catch { }

            return new { Data = ApiFailureString };
        }

        [HttpPost]
        [Route("UpdateLocation")]
        public dynamic UpdateLocation([FromBody] InputModel inputModel)
        {
            try
            {
                // var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(inputModel.Data);
                //var encryptedData = EncryptDecryptUtils.Encrypt(PhraseKey, inputModel.Data);
                UserLocationModel model = JsonConvert.DeserializeObject<UserLocationModel>(EncryptDecryptUtils.Decrypt(PhraseKey, inputModel.Data));

                var outputerr1 = JsonConvert.SerializeObject(new { Result = "failed", Error = "UserId not valid" });
                if (model.UserId == null || model.UserId == 0)
                {
                    return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, outputerr1) };
                }
                var outputerr = JsonConvert.SerializeObject(new { Result = "failed",Error="Lat or Lng are not valid" });
                if (string.IsNullOrEmpty(model.Lat) || string.IsNullOrEmpty(model.Lng))
                {
                    return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, outputerr) };
                }
                var userDetails = _vwContext.Users.Where(x => x.UserId == model.UserId).FirstOrDefault();

                userDetails.Lat = model.Lat;
                userDetails.Lng = model.Lng;
                userDetails.LocModifyDate = DateTime.Now;
                _vwContext.Users.Update(userDetails);
                _vwContext.SaveChanges();

                var output = JsonConvert.SerializeObject(new { Result = "success" });
                return new { Data = EncryptDecryptUtils.Encrypt(PhraseKey, output) };
            }
            catch(Exception ex) 
            { 
            
            }

            return new { Data = ApiFailureString };
        }

        #endregion
    }
}
