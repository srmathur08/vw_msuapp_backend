﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using VW.MSU.Common;
using VW.MSU.Data.Models;

namespace VW.MSU.UI.Controllers.Api
{
    public class ApiBaseController : ControllerBase
    {
        protected readonly IWebHostEnvironment _hostingEnvironment;
        protected readonly IConfiguration _config;
        protected readonly VWContext _vwContext;
        protected readonly string PhraseKey;
        protected readonly string ApiFailureString;
        protected readonly string[] VideoExts;

        public ApiBaseController(IWebHostEnvironment hostingEnvironment, IConfiguration config, VWContext vwContext)
        {
            _hostingEnvironment = hostingEnvironment;
            _config = config;
            _vwContext = vwContext;

            PhraseKey = _config.GetValue<string>("PhraseKey");
            VideoExts = _config.GetValue<string>("VideoExt").Split(",", System.StringSplitOptions.RemoveEmptyEntries);
            ApiFailureString = EncryptDecryptUtils.Encrypt(PhraseKey, JsonConvert.SerializeObject(new { Result = "failure" }));
        }
    }
}
