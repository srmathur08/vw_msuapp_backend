﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using VW.MSU.Common;
using VW.MSU.Common.Helpers;
using VW.MSU.Data.Models;
using VW.MSU.Models;
using VW.MSU.Services;
using VW.MSU.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.IO;
using System.Globalization;

namespace VW.MSU.UI.Controllers
{
    [Authorize]
    [SessionExpire]
    public class HomeController : BaseController
    {
        private readonly string DefaultLoginCookie = "VW.MSU.AuthCookieAspNetCore";
        public readonly ILogger<HomeController> _logger;
        public SMTPSettings _smtpSettings;

        public HomeController(IWebHostEnvironment hostingEnvironment, ILogger<HomeController> logger, IConfiguration config, VWContext vwContext, IOptions<SMTPSettings> smtpSettings, IOptions<SFDCSettings> sfdcSettings)
            : base(hostingEnvironment, config, vwContext, sfdcSettings.Value)
        {
            _logger = logger;
            _smtpSettings = smtpSettings.Value;
        }

        #region Methods

        #endregion

        #region Login

        public bool IsAuthenticated
        {
            get
            {
                if (HttpContext.Session.TryGetValue("UserDetails", out _))
                {
                    return HttpContext.User.Identity.IsAuthenticated;
                }
                else
                    return false;
            }
        }

        [AllowAnonymous]
        public IActionResult Login(string ReturnUrl)
        {
            if (IsAuthenticated)
            {
                if (!string.IsNullOrWhiteSpace(ReturnUrl) && Url.IsLocalUrl(ReturnUrl))
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            LoginModel cm = new LoginModel
            {
                ReturnUrl = ReturnUrl
            };

            if (HttpContext.Request.Cookies[DefaultLoginCookie] != null)
            {
                var cookieValue = HttpContext.Request.Cookies.Where(x => x.Key.Equals(DefaultLoginCookie)).FirstOrDefault().Value;
                UserDetails userDetail = JsonConvert.DeserializeObject<UserDetails>(EncryptDecryptUtils.Decrypt(PhraseKey, cookieValue));
                cm.PhoneNo = userDetail.PhoneNo;
            }

            return View(cm);
        } 
         
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel loginModel)
        {
            try
            {
                if (await LoginUser(loginModel))
                {
                    if (!string.IsNullOrWhiteSpace(loginModel.ReturnUrl) && Url.IsLocalUrl(loginModel.ReturnUrl))
                    {
                        return Redirect(loginModel.ReturnUrl);
                    }

                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                loginModel.ErrorMessage = ex.Message;
            }

            return View(loginModel);
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            HttpContext.Session.Clear();

            return RedirectToAction("Login");
        }

        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }
        
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Forgotpassword()
        {
            LoginModel model = new LoginModel();
            return View(model);
        }
        
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Forgotpassword(LoginModel model)
        {
            try
            {
                var user = _vwContext.Users.Where(x => EF.Functions.Like(x.Phone, model.PhoneNo)).FirstOrDefault(); 
                var encPassword = EncryptDecryptUtils.Decrypt(PhraseKey, user.Password);
                EmailServices.SendMail(_smtpSettings.Host, _smtpSettings.Port, _smtpSettings.Username, _smtpSettings.Password, _smtpSettings.EnableSsl, _smtpSettings.FromAddress, _smtpSettings.FromName, user.Email, "Hello " + user.Fullname + ",<br/><br/>" + "Your Password is:" + encPassword.ToString() + " <br/><br/>Thanks,<br/>Skoda PCMP Team", "Forgot Password", true, null);
                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }
            catch (Exception)
            {
            }

            return View(model);
        }

        private async Task<bool> LoginUser(LoginModel model)
        {
            bool completed = false;
            var encPassword = EncryptDecryptUtils.Encrypt(PhraseKey, model.Password);
            var user = _vwContext.Users.Where(x => EF.Functions.Like(x.Phone, model.PhoneNo) && EF.Functions.Like(x.Password, encPassword)).FirstOrDefault();

            if (user != null)
            {
                var uInfo = new UserDetails()
                {
                    PhoneNo = model.PhoneNo,
                    Password = EncryptDecryptUtils.Encrypt(PhraseKey, model.Password),
                    Fullname = user.Fullname,
                    Role = user.Role?.Name,
                    CMSRole = user.Cmsrole?.Name,
                    RoleId = user.RoleId,
                    CMSRoleId = user.CmsroleId,
                    DealershipId = user.DealershipId.HasValue ? user.DealershipId.Value : 0,
                    GroupDealerId = user.GroupDealerId.HasValue ? user.GroupDealerId.Value : 0,
                    UserId = user.UserId,
                };

                string currentUser = EncryptDecryptUtils.Encrypt(PhraseKey, JsonConvert.SerializeObject(uInfo));

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserId.ToString())
                };

                ClaimsIdentity userIdentity = new ClaimsIdentity(claims, "login");
                ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);

                await HttpContext.SignInAsync(principal);

                if (model.RememberMe)
                {
                    if (HttpContext.Request.Cookies[DefaultLoginCookie] != null)
                    {
                        Response.Cookies.Append(DefaultLoginCookie, currentUser);
                    }
                    else
                    {
                        Response.Cookies.Append(DefaultLoginCookie, currentUser, new Microsoft.AspNetCore.Http.CookieOptions() { Expires = DateTime.Now.AddYears(100) });
                    }
                }

                HttpContext.Session.Set("UserDetails", ObjectToByte.ObjectToByteArray(uInfo));
                completed = true;
            }
            else
                model.ErrorMessage = "Invalid credentails";

            return completed;
        }

        #endregion

        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult SR(long srId)
        {
            //long srId = Convert.ToInt64(EncryptDecryptUtils.Decrypt(PhraseKey, sr));
            var serviceRequest = _vwContext.ServiceRequest.Where(x => x.Srid == srId).FirstOrDefault();
            var model = new ServiceRequestModel
            {
                Srid = serviceRequest.Srid,
                Srnumber = serviceRequest.Srnumber,
                Vinnumber = serviceRequest.Vinnumber,
                CustomerName = serviceRequest.CustomerName,
                VehicleModel = serviceRequest.VehicleModelId.HasValue ? serviceRequest.VehicleModelNavigation.Name : string.Empty,// x.VehicleModel,
                VariantName = serviceRequest.VariantName,
                InvoiceAmount = serviceRequest.InvoiceAmount,
                Duration = serviceRequest.Duration,
                IsOilChange = serviceRequest.IsOilChange,
                IsBrakes = serviceRequest.IsBrakes,
                IsTyreCheck = serviceRequest.IsTyreCheck,
                IsBatteryCheck = serviceRequest.IsBatteryCheck,
                ServiceDate = serviceRequest.ServiceDateTime.ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture),
                Lat = serviceRequest.Lat,
                Lng = serviceRequest.Lng,
                RegNum = serviceRequest.RegNum,
                OdometerReading = serviceRequest.OdometerReading,
                ServiceType = serviceRequest.ServiceType,
                VehicleImage = serviceRequest.VehicleModelId.HasValue ? string.Format("~{0}", serviceRequest.VehicleModelNavigation.Image) : string.Empty,
                VehicleModelData = serviceRequest.VehicleModelId.HasValue ? new ModelsModel
                {
                    BackAngleImage = string.Format("~{0}", serviceRequest.VehicleModelNavigation.BackAngleImage),
                    RightAngleImage = string.Format("~{0}", serviceRequest.VehicleModelNavigation.RightAngleImage),
                    LeftAngleImage = string.Format("~{0}", serviceRequest.VehicleModelNavigation.LeftAngleImage),
                    FrontAngleImage = string.Format("~{0}", serviceRequest.VehicleModelNavigation.FrontAngleImage),
                } : null,
                VehicleCheckup = serviceRequest.VehicleCheckup.Select(x => new ResponseVehicleCheckupModel
                {
                    Id = x.Id,
                    Srid = x.Srid,
                    FuelLevel = x.FuelLevel.HasValue ? x.FuelLevel.Value : 0,
                    VehicleMileage = x.VehicleMileage,
                    VehicleCheckupAssets = x.VehicleCheckupAsset.OrderBy(y => y.CreatedDate).Select(y => new VehicleCheckupAssetModel
                    {
                        Asset = string.Format("~{0}", y.Asset),
                        Id = y.Id,
                        Thumb = string.IsNullOrEmpty(y.Thumb) ? "" : string.Format("~{0}", y.Thumb),
                        IsVideo = VideoExts.Contains(Path.GetExtension(y.Asset))
                    }).ToList()
                }).FirstOrDefault(),
                VehicleCheckupSecData = serviceRequest.VehicleCheckup.FirstOrDefault().VehicleCheckupData.GroupBy(z => z.SectionName).Select(z => new VehicleCheckupSecData
                {
                    SectionName = z.Key,
                    SectionNameKey = z.Key.Replace(" ", "").ToLower(),
                    Percent = CalculatePercent(z.ToList()),
                    VehicleCheckupData = z.Select(a =>
                      new ResponseVehicleCheckupDataModel
                      {
                          Name = a.Name,
                          SectionName = a.SectionName,
                          Value = a.Value,
                          Id = a.Id,
                          Remarks = a.Remarks,
                          VehicleCheckupId = a.VehicleCheckupId,
                          Status = a.Status,
                          Assets = a.VehicleCheckupDataAsset.Select(b => new VehicleCheckupDataAssetModel
                          {
                              Asset = string.Format("~{0}", b.Asset),
                              Thumb = string.IsNullOrEmpty(b.Thumb) ? "" : string.Format("~{0}", b.Thumb),
                              Id = b.Id,
                              VehicleCheckupDataId = b.VehicleCheckupDataId,
                              IsVideo = VideoExts.Contains(Path.GetExtension(b.Asset))
                          }).ToList()
                      }).ToList()
                }).ToList()
            };

            return View(model);
        }

        public int CalculatePercent(List<VehicleCheckupData> vcds)
        {
            var totalPoints = vcds.Count() * 2;
            var redPoints = vcds.Where(x => x.Status == 1).Count() * 0;
            var yellowPoints = vcds.Where(x => x.Status == 2).Count() * 1;
            var greenPoints = vcds.Where(x => x.Status == 3).Count() * 2;

            var percentpoint = (greenPoints + redPoints + yellowPoints) / totalPoints;

            return percentpoint;
        }

        #region Ajax APIs

        [HttpPost]
        public JsonResult GetDealershipsByGroupDealer(int Id)
        {
            if (Id > 0)
                return new JsonResult(_vwContext.Dealership.Where(x => x.GroupDealerId == Id && !x.IsDeleted && x.IsActive).Select(x => new { Id = x.DealershipId, x.Name }));
            else
                return new JsonResult(_vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && !x.GroupDealerId.HasValue).Select(x => new { x.Name, Id = x.DealershipId }));
        }

        [HttpPost]
        public JsonResult VerifyVIN(string VIN)
        {
            try
            {
                TokenModel token = null;
                if (HttpContext.Session.TryGetValue("Token", out byte[] TokeyBytes))
                {
                    token = ObjectToByte.ByteArrayToObject<TokenModel>(TokeyBytes);
                }
                else
                {
                    token = GenerateToken();
                }

                var vindDataUrl = string.Format(_sfdcSettings.VinDataUrl, token.instance_url, VIN);

                NameValueCollection headers = new NameValueCollection();
                headers.Add("Authorization", string.Format("{0} {1}", token.token_type, token.access_token));

                var response = WebRequestHelper.GET(vindDataUrl, headers: headers);
                if (response.Status == System.Net.HttpStatusCode.OK)
                {
                    var vinData = JsonConvert.DeserializeObject<VinDataModel>(response.Response);
                    return new JsonResult(new { result = "success", vinData });
                }
                else if (response.Status == System.Net.HttpStatusCode.NotFound)
                {
                    return new JsonResult(new { result = "failure", Error = "NOT_FOUND" });
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Verify VIN failed");
            }

            return new JsonResult(new { result = "failure" }); ;
        }

        #endregion
    }
}