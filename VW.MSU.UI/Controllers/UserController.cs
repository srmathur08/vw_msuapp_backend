﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using VW.MSU.Common;
using VW.MSU.Common.Enums;
using VW.MSU.Data.Models;
using VW.MSU.Models;
using VW.MSU.UI.Models;

namespace VW.MSU.UI.Controllers
{
    [Authorize]
    [SessionExpire]
    public class UserController : BaseController
    {
        public readonly ILogger<UserController> _logger;

        public UserController(IWebHostEnvironment hostingEnvironment, ILogger<UserController> logger, IConfiguration config, VWContext vwContext, IOptions<SFDCSettings> sfdcSettings)
            : base(hostingEnvironment, config, vwContext, sfdcSettings.Value)
        {
            _logger = logger;
        }

        #region Methods

        #endregion

        #region Create or Update User

        public IActionResult CreateOrUpdate(long id = 0)
        {
            UserDetails userDetails = GetUserDetails();

            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)))
            {
                UserModel model;

                if (id > 0)
                {
                    var user = _vwContext.Users.Where(x => x.UserId == id).FirstOrDefault();
                    if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                    {
                        if (user.GroupDealerId != userDetails.GroupDealerId)
                            return RedirectToAction("Index", "Home");
                    }
                    else if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
                    {
                        if (user.DealershipId != userDetails.DealershipId )
                            return RedirectToAction("Index", "Home");
                    }

                    model = new UserModel
                    {
                        UserId = user.UserId,
                        CmsroleId = user.CmsroleId.HasValue ? user.CmsroleId.Value : 0,
                        DealershipId = user.DealershipId.HasValue ? user.DealershipId.Value : 0,
                        Email = user.Email,
                        Fullname = user.Fullname,
                        Phone = user.Phone,
                        RoleId = user.RoleId,
                        GroupDealerId = user.GroupDealerId.HasValue ? user.GroupDealerId.Value : 0
                    };

                    if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                    {
                        model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && x.GroupDealerId == userDetails.GroupDealerId).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = user.DealershipId.HasValue && user.DealershipId.Value == x.DealershipId }).ToList();
                        model.CMSRoles = _vwContext.Cmsroles.Where(x => x.CmsroleId == 4 || x.CmsroleId == 5 || x.CmsroleId == 6).Select(x => new SelectListItem { Value = x.CmsroleId.ToString(), Text = x.Name, Selected = user.CmsroleId.HasValue && user.CmsroleId.Value == x.CmsroleId }).ToList();
                    }
                    else if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
                    {
                        model.CMSRoles = _vwContext.Cmsroles.Where(x => x.CmsroleId == 6 || x.CmsroleId == 5).Select(x => new SelectListItem { Value = x.CmsroleId.ToString(), Text = x.Name, Selected = user.CmsroleId.HasValue && user.CmsroleId.Value == x.CmsroleId }).ToList();
                    }
                    else
                    {
                        model.CMSRoles = _vwContext.Cmsroles.Where(x => x.CmsroleId != (int)CMSRoleTypes.SUPERADMIN).Select(x => new SelectListItem { Value = x.CmsroleId.ToString(), Text = x.Name, Selected = user.CmsroleId.HasValue && user.CmsroleId.Value == x.CmsroleId }).ToList();
                        if (model.GroupDealerId > 0)
                            model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && x.GroupDealerId == model.GroupDealerId).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = user.DealershipId.HasValue && user.DealershipId.Value == x.DealershipId }).ToList();
                        else
                            model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && !x.GroupDealerId.HasValue).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = user.DealershipId.HasValue && user.DealershipId.Value == x.DealershipId }).ToList();
                        model.Roles = _vwContext.Roles.Select(x => new SelectListItem { Value = x.RoleId.ToString(), Text = x.Name, Selected = user.RoleId.HasValue && user.RoleId.Value == x.RoleId }).ToList();
                        model.GroupDealers = _vwContext.GroupDealer.Where(z => !z.IsDeleted && z.IsActive).Select(z => new SelectListItem { Text = z.Name, Value = z.GroupDealerId.ToString(), Selected = user.GroupDealerId == z.GroupDealerId }).ToList();
                    }
                }
                else
                {
                    model = new UserModel() { GroupDealerId = userDetails.GroupDealerId, DealershipId = userDetails.DealershipId };

                    if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                    {
                        model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && x.GroupDealerId == userDetails.GroupDealerId).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString() }).ToList();
                        model.CMSRoles = _vwContext.Cmsroles.Where(x => x.CmsroleId == 6 || x.CmsroleId == 4 || x.CmsroleId == 5).Select(x => new SelectListItem { Value = x.CmsroleId.ToString(), Text = x.Name }).ToList();
                    }
                    else if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
                    {
                        model.CMSRoles = _vwContext.Cmsroles.Where(x => x.CmsroleId == 6 || x.CmsroleId == 5).Select(x => new SelectListItem { Value = x.CmsroleId.ToString(), Text = x.Name }).ToList();
                    }
                    else
                    {
                        model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && !x.GroupDealerId.HasValue).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString() }).ToList();
                        model.Roles = _vwContext.Roles.Select(x => new SelectListItem { Value = x.RoleId.ToString(), Text = x.Name }).ToList();
                        model.CMSRoles = _vwContext.Cmsroles.Where(x => x.CmsroleId != (int)CMSRoleTypes.SUPERADMIN).Select(x => new SelectListItem { Value = x.CmsroleId.ToString(), Text = x.Name }).ToList();
                        model.GroupDealers = userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN ? model.GroupDealers : _vwContext.GroupDealer.Where(z => !z.IsDeleted && z.IsActive).Select(z => new SelectListItem { Text = z.Name, Value = z.GroupDealerId.ToString() }).ToList();
                    }
                }

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public IActionResult CreateOrUpdate(UserModel model)
        {
            UserDetails userDetails = GetUserDetails();

            try
            {
                var userId = int.Parse(HttpContext.User.Identity.Name ?? "-1");
                var now = DateTime.Now;

                Users user;
                if (model.UserId > 0)
                {
                    user = _vwContext.Users.Where(x => x.UserId == model.UserId).FirstOrDefault();
                }
                else
                {
                    user = new Users
                    {
                        CreatedBy = userId,
                        CreatedDate = now,
                        IsDeleted = false,
                        IsActive = true
                    };
                }

                user.Fullname = model.Fullname;
                user.Phone = model.Phone;
                user.Email = model.Email;

                if (model.CmsroleId > 0)
                    user.CmsroleId = model.CmsroleId;
                if (model.DealershipId > 0)
                    user.DealershipId = model.DealershipId;
                if (model.GroupDealerId > 0)
                    user.GroupDealerId = model.GroupDealerId;
                if (!string.IsNullOrEmpty(model.Password))
                    user.Password = EncryptDecryptUtils.Encrypt(PhraseKey, model.Password);
                if (model.RoleId > 0)
                    user.RoleId = model.RoleId;

                if (model.UserId > 0)
                    _vwContext.Users.Update(user);
                else
                    _vwContext.Users.Add(user);

                _vwContext.SaveChanges();

                //if (model.ImageBlob != null && model.ImageBlob.Length > 0)
                //{
                //    var assetPath = $"{_hostingEnvironment.WebRootPath}/Content/Dealership/{user.DealershipId}";
                //    var uploadFile = ConvertUploadFileToBase64String(model.ImageBlob);
                //    UtilityServices.UploadBase64String(assetPath, $"{user.DealershipId}{Path.GetExtension(model.ImageBlob.FileName)}", uploadFile, _hostingEnvironment.WebRootPath, false);
                //    user.Image = $"/Content/Dealership/{user.DealershipId}/{user.DealershipId}{Path.GetExtension(model.ImageBlob.FileName)}";
                //}

                //_vwContext.Dealership.Update(user);
                //_vwContext.SaveChanges();

                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                model.ErrorMessage = ex.Message;
            }

            if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
            {
                model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && x.GroupDealerId == userDetails.GroupDealerId).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = model.DealershipId.HasValue && model.DealershipId.Value == x.DealershipId }).ToList();
                model.CMSRoles = _vwContext.Cmsroles.Where(x => x.CmsroleId == 4 || x.CmsroleId == 5 || x.CmsroleId == 6).Select(x => new SelectListItem { Value = x.CmsroleId.ToString(), Text = x.Name, Selected = model.CmsroleId.HasValue && model.CmsroleId.Value == x.CmsroleId }).ToList();
            }
            else if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
            {
                model.CMSRoles = _vwContext.Cmsroles.Where(x => x.CmsroleId == 6 || x.CmsroleId == 5).Select(x => new SelectListItem { Value = x.CmsroleId.ToString(), Text = x.Name, Selected = model.CmsroleId.HasValue && model.CmsroleId.Value == x.CmsroleId }).ToList();
            }
            else
            {
                model.CMSRoles = _vwContext.Cmsroles.Where(x => x.CmsroleId != (int)CMSRoleTypes.SUPERADMIN).Select(x => new SelectListItem { Value = x.CmsroleId.ToString(), Text = x.Name, Selected = model.CmsroleId.HasValue && model.CmsroleId.Value == x.CmsroleId }).ToList();
                if (model.GroupDealerId > 0)
                    model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && x.GroupDealerId == model.GroupDealerId).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = model.DealershipId.HasValue && model.DealershipId.Value == x.DealershipId }).ToList();
                else
                    model.Dealerships = _vwContext.Dealership.Where(x => !x.IsDeleted && x.IsActive && !x.GroupDealerId.HasValue).Select(x => new SelectListItem { Text = x.Name, Value = x.DealershipId.ToString(), Selected = model.DealershipId.HasValue && model.DealershipId.Value == x.DealershipId }).ToList();
                model.Roles = _vwContext.Roles.Select(x => new SelectListItem { Value = x.RoleId.ToString(), Text = x.Name, Selected = model.RoleId.HasValue && model.RoleId.Value == x.RoleId }).ToList();
                model.GroupDealers = _vwContext.GroupDealer.Where(z => !z.IsDeleted && z.IsActive).Select(z => new SelectListItem { Text = z.Name, Value = z.GroupDealerId.ToString(), Selected = model.GroupDealerId == z.GroupDealerId }).ToList();
            }

            return View(model);
        }

        #endregion

        public IActionResult List()
        {
            UserDetails userDetails = GetUserDetails();
            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)))
            {
                List<UserModel> model;
                if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                {
                    model = _vwContext.Users.Where(x => x.GroupDealerId == userDetails.GroupDealerId && !x.IsDeleted).OrderByDescending(x => x.CreatedDate).Select(x => new UserModel()
                    {
                        Fullname = x.Fullname,
                        Email = x.Email,
                        UserId = x.UserId,
                        IsActive = x.IsActive
                    }).ToList();
                }
                else if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
                {
                    model = _vwContext.Users.Where(x => x.DealershipId == userDetails.DealershipId && !x.IsDeleted).OrderByDescending(x => x.CreatedDate).Select(x => new UserModel()
                    {
                        Fullname = x.Fullname,
                        Email = x.Email,
                        UserId = x.UserId,
                        IsActive = x.IsActive
                    }).ToList();
                }
                else
                {
                    model = _vwContext.Users.Where(x => !x.IsDeleted && (!x.CmsroleId.HasValue || (x.CmsroleId.HasValue && x.CmsroleId.Value != (int)CMSRoleTypes.SUPERADMIN))).OrderByDescending(x => x.CreatedDate).Select(x => new UserModel()
                    {
                        Fullname = x.Fullname,
                        Email = x.Email,
                        UserId = x.UserId,
                        IsActive = x.IsActive
                    }).ToList();
                }

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public IActionResult Delete(long id)
        {
            UserDetails userDetails = GetUserDetails();
            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
            (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)))
            {
                Users user;
                if (userDetails.CMSRoleId.HasValue && userDetails.CMSRoleId.Value == (int)CMSRoleTypes.GROUPDEALERADMIN)
                {
                    user = _vwContext.Users.Where(x => x.UserId == id && x.GroupDealerId == userDetails.GroupDealerId).FirstOrDefault();
                }
                else if (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.DEALERSHIPADMIN)
                {
                    user = _vwContext.Users.Where(x => x.UserId == id && x.DealershipId == userDetails.DealershipId).FirstOrDefault();
                }
                else {
                    user = _vwContext.Users.Where(x => x.UserId == id).FirstOrDefault();
                }

                if (user != null)
                {
                    user.IsDeleted = true;
                    _vwContext.Users.Update(user);
                    _vwContext.SaveChanges();
                }

                return RedirectToAction("List");
            }
            else
                return RedirectToAction("Index", "Home");
        }
    }
}
