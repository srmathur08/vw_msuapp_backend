﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using VW.MSU.Common;
using VW.MSU.Common.Enums;
using VW.MSU.Data.Models;
using VW.MSU.Models;
using VW.MSU.UI.Models;

namespace VW.MSU.UI.Controllers
{
    [Authorize]
    [SessionExpire]
    public class GroupDealerController : BaseController
    {
        public readonly ILogger<GroupDealerController> _logger;

        public GroupDealerController(IWebHostEnvironment hostingEnvironment, ILogger<GroupDealerController> logger, IConfiguration config, VWContext vwContext, IOptions<SFDCSettings> sfdcSettings)
            : base(hostingEnvironment, config, vwContext, sfdcSettings.Value)
        {
            _logger = logger;
        }

        #region Methods

        #endregion

        #region Create or Update GroupDealer

        public IActionResult CreateOrUpdate(long id = 0)
        {
            UserDetails userDetails = GetUserDetails();

            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN)))
            {
                GroupDealerModel model;
                if (id > 0)
                {
                    var grpDealer = _vwContext.GroupDealer.Where(x => x.GroupDealerId == id).FirstOrDefault();
                    model = new GroupDealerModel
                    {
                        GroupDealerId = grpDealer.GroupDealerId,
                        Name = grpDealer.Name,
                        GroupDealerUsers = grpDealer.GroupDealerUsers.Where(x => !x.IsDeleted).OrderBy(x => x.CreatedDate).Select(x => new GroupDealerUserModel() { Email = x.Email, Fullname = x.Fullname, Phone = x.Phone, Id = x.Id }).ToList()
                    };
                }
                else
                    model = new GroupDealerModel();

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public IActionResult CreateOrUpdate(GroupDealerModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userId = int.Parse(HttpContext.User.Identity.Name ?? "-1");
                    var now = DateTime.Now;

                    GroupDealer grpDealer;
                    if (model.GroupDealerId > 0)
                    {
                        grpDealer = _vwContext.GroupDealer.Where(x => x.GroupDealerId == model.GroupDealerId).FirstOrDefault();
                    }
                    else
                    {
                        grpDealer = new GroupDealer
                        {
                            CreatedBy = userId,
                            CreatedDate = now,
                            IsDeleted = false,
                            IsActive = true
                        };
                    }

                    grpDealer.Name = model.Name;

                    if (model.GroupDealerId > 0)
                        _vwContext.GroupDealer.Update(grpDealer);
                    else
                        _vwContext.GroupDealer.Add(grpDealer);

                    _vwContext.SaveChanges();

                    List<GroupDealerUsers> grpDealerUsers = new List<GroupDealerUsers>();
                    foreach (var grpDealerUser in model.GroupDealerUsers)
                    {
                        if (grpDealerUser.Id <= 0 && grpDealerUser.IsDeleted)
                        {

                        }
                        else
                        {
                            GroupDealerUsers groupDealerUser;
                            if (grpDealerUser.Id > 0)
                            {
                                groupDealerUser = _vwContext.GroupDealerUsers.Where(x => x.Id == grpDealerUser.Id).FirstOrDefault();
                            }
                            else
                            {
                                groupDealerUser = new GroupDealerUsers() { CreatedBy = userId, CreatedDate = DateTime.Now };
                            }

                            groupDealerUser.Email = grpDealerUser.Email;
                            groupDealerUser.Phone = grpDealerUser.Phone;
                            groupDealerUser.Fullname = grpDealerUser.Fullname;
                            groupDealerUser.GroupDealerId = grpDealer.GroupDealerId;
                            if (grpDealerUser.IsDeleted)
                            {
                                groupDealerUser.IsDeleted = true;
                            }

                            if (grpDealerUser.Id > 0)
                                _vwContext.GroupDealerUsers.Update(groupDealerUser);
                            else
                                grpDealerUsers.Add(groupDealerUser);
                        }
                    }

                    if (grpDealerUsers.Count() > 0)
                    {
                        _vwContext.GroupDealerUsers.AddRange(grpDealerUsers);
                    }

                    _vwContext.SaveChanges();

                    return RedirectToAction("List");
                }
                catch (Exception ex)
                {
                    model.ErrorMessage = ex.Message;
                }
            }

            return View(model);
        }

        #endregion

        public IActionResult List()
        {
            UserDetails userDetails = GetUserDetails();
            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
                (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN)))
            {
                var model = _vwContext.GroupDealer.Where(x => !x.IsDeleted).OrderByDescending(x => x.CreatedDate).Select(x => new GroupDealerModel()
                {
                    Name = x.Name,
                    GroupDealerId = x.GroupDealerId,
                    IsActive = x.IsActive,
                    DealershipCount = x.Dealership.Where(y => !y.IsDeleted).Count()
                }).ToList();

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public IActionResult Delete(long id)
        {
            UserDetails userDetails = GetUserDetails();
            if ((userDetails.RoleId.HasValue && userDetails.RoleId.Value > 0) ||
            (userDetails.CMSRoleId.HasValue && (userDetails.CMSRoleId.Value == (int)CMSRoleTypes.VWADMIN || userDetails.CMSRoleId.Value == (int)CMSRoleTypes.SUPERADMIN)))
            {
                var grpDealer = _vwContext.GroupDealer.Where(x => x.GroupDealerId == id).FirstOrDefault();
                grpDealer.IsDeleted = true;
                _vwContext.GroupDealer.Update(grpDealer);
                _vwContext.SaveChanges();

                return RedirectToAction("List");
            }
            else
                return RedirectToAction("Index", "Home");
        }
    }
}
