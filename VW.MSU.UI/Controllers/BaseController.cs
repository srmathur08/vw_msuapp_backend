﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.IO;
using VW.MSU.Common.Helpers;
using VW.MSU.Data.Models;
using VW.MSU.Models;
using VW.MSU.UI.Models;

namespace VW.MSU.UI.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IWebHostEnvironment _hostingEnvironment;
        protected readonly IConfiguration _config;
        protected readonly VWContext _vwContext;
        public SFDCSettings _sfdcSettings;
        protected readonly string PhraseKey;
        protected readonly string AppUrl;
        protected readonly string GoogleMapAPIKey;
        protected readonly string[] VideoExts;

        public BaseController(IWebHostEnvironment hostingEnvironment, IConfiguration config, VWContext vwContext, SFDCSettings sfdcSettings)
        {
            _hostingEnvironment = hostingEnvironment;
            _config = config;
            _sfdcSettings = sfdcSettings;
            _vwContext = vwContext;
            PhraseKey = _config.GetValue<string>("PhraseKey");
            GoogleMapAPIKey = _config.GetValue<string>("GoogleMapAPIKey");
            AppUrl = _config.GetValue<string>("AppUrl");
            VideoExts = _config.GetValue<string>("VideoExt").Split(",", System.StringSplitOptions.RemoveEmptyEntries);
        }

        public TokenModel GenerateToken()
        {
            var response = WebRequestHelper.POST(_sfdcSettings.LoginUrl, string.Empty);
            if (response.Status == System.Net.HttpStatusCode.OK)
            {
                var result = JsonConvert.DeserializeObject<TokenModel>(response.Response);
                HttpContext.Session.Set("Token", ObjectToByte.ObjectToByteArray(result));

                return result;
            }

            return null;
        }

        public UserDetails GetUserDetails()
        {
            if (HttpContext.Session.TryGetValue("UserDetails", out byte[] userDetailsBytes))
            {
                return ObjectToByte.ByteArrayToObject<UserDetails>(userDetailsBytes);
            }

            return null;
        }

        public string ConvertUploadFileToBase64String(IFormFile file)
        {
            var uploadFile = string.Empty;

            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                var fileBytes = ms.ToArray();
                uploadFile = Convert.ToBase64String(fileBytes);
            }

            return uploadFile;
        }
    }
}
