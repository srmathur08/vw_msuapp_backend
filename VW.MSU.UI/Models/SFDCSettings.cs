﻿namespace VW.MSU.UI.Models
{
    public class SFDCSettings
    {
        public string LoginUrl { get; set; }
        public string VinDataUrl { get; set; }
    }
}
