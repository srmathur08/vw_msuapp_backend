﻿namespace VW.MSU.UI.Models
{
    public class NotificationSettings
    {
        public string RequestUrl { get; set; }
        public string ApiKey { get; set; }
        public string SenderId { get; set; }
        public string Priority { get; set; }
    }
}
