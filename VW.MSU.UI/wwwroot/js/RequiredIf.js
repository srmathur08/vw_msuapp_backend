﻿function requiredif(value, element, param) {
    var hasValue = false;

    if ($(element)[0].files == null && value != "")
        return true;
    if ($(element)[0].files != null && $(element)[0].files.length > 0)
        return true;

    var otherPropId = $(element).data('val-other');
    if (otherPropId) {
        var otherProp = $(otherPropId);
        if (otherProp) {
            var otherVal = otherProp.val();
            if (otherVal > 0) {
                return true;
            }
        }
    }
    return false;
}

$.validator.addMethod("requiredif", requiredif);
$.validator.unobtrusive.adapters.addBool("requiredif");