﻿$(document).ready(function () {
    $(".dealership-type").on("click", function () {
        var dealershipType = $("input[name='DealershipType']:checked").val();
        if (dealershipType == 'Individual') {
            $(".grp-dealer").hide();
            $(".grpDealerDDL option[value='0']").prop('selected', true);
        }
        else {
            $(".grp-dealer").show();
        }
    });
});
