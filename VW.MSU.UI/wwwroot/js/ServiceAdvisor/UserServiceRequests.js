﻿
var map, markers, bounds, inowindowcontent = '', counter = 0;
let infoWindoPromise;


function initMap() {
    var mapelement = document.getElementById("map");
    if (mapelement) {
        map = new google.maps.Map(mapelement, {
            mapTypeControl: false,
            center: { lat: 17.383950000000002, lng: 78.46724 },
            zoom: 11
        });
        bounds = new google.maps.LatLngBounds();
    }
   
}

function plotMarkers(serviceModel) {
    var srs = serviceModel.serviceRequests;
    var userLocation = serviceModel.assigneeDetails;
    srs.push({
        lat: userLocation.lat,
        lng: userLocation.lng,
        customerName: 'Live location',
        srid:-1

    })
    var serviceRequests = srs.filter(x => x.lat && x.lng);
    for (var i = 0; i < serviceRequests.length; i++) {
        // get current location
        var sr = serviceRequests[i];

        // create map position        
        if (sr.lat != null || sr.lng != null) {
            var position = new google.maps.LatLng(sr.lat, sr.lng);
            // add position to bounds
            bounds.extend(position);

            // create marker
            var marker = new google.maps.Marker({
                animation: google.maps.Animation.DROP
                , icon: sr.srid == -1 ? "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png" : undefined
                , map: map
                , position: position
                , title: sr.customerName
                , data: sr
            }).addListener("click", function (event) {
                inowindowcontent = '';
                counter = 0;
                var geocoder = new google.maps.Geocoder();
                let clickedMarker = this;
                geocoder.geocode({ location: event.latLng }).then((response) => {
                    if (response.results[0]) {
                        let infoWindoPromise1 = new Promise(function (infoResolve, infoReject) {
                            getInfoWindowContent(response.results[0].formatted_address, clickedMarker.data, serviceRequests, infoResolve, infoReject);

                        });
                        infoWindoPromise1.then(function (test) {
                            //alert(inowindowcontent);
                            var infowindow = new google.maps.InfoWindow();
                            infowindow.setContent(test);
                            infowindow.open(map, clickedMarker);
                        },
                            function (error) {
                                //return 'error33'
                                //alert(error);
                            });

                    }
                });
            });

            // create info window and add to marker
            map.fitBounds(bounds);
        }
        else {
            alert("Invalid Lat, Lng for " + sr.customerName);
        }
        
    };

    function getInfoWindowContent(formatedaddress, source, destinations, infoResolve, infoReject) {
        var infoWindowText = '';
        for (var i = 0; i < destinations.length; i++) {
            if (destinations[i].srid != source.srid && destinations[i].lat != null && destinations[i].lng!=null) {
                const sourceLoc = { lat: parseFloat(source.lat), lng: parseFloat(source.lng) };
                const destLoc = { lat: parseFloat(destinations[i].lat), lng: parseFloat(destinations[i].lng) };
                let myPromise = new Promise(function (myResolve, myReject) {
                    var tmp = getDistanceAndDuration(sourceLoc, destLoc, source, destinations[i], myResolve, myReject);
                });

                myPromise.then(
                    function (durationdata) {
                        counter++;
                        inowindowcontent = inowindowcontent + durationdata;
                        if (counter >= destinations.length-1) {
                            infoResolve(inowindowcontent);
                        }

                    },
                    function (error) {
                        infoReject('error')
                        //alert(error);
                    }
                );
            }
        }
    }


    function getDistanceAndDuration(source, destination, src, dest, myResolve, myReject) {
        var infotxt = '';
        let directionsService = new google.maps.DirectionsService();
        let directionsRenderer = new google.maps.DirectionsRenderer();
        directionsRenderer.setMap(map); // Existing map object displays directions
        // Create route from existing points used for markers
        const route = {
            origin: source,
            destination: destination,
            travelMode: 'DRIVING'
        }

        let returndata = ""
        directionsService.route(route,
            function (response, status) { // anonymous function to capture directions
                if (status !== 'OK') {
                    window.alert('Directions request failed due to ' + status);
                    //return;
                    myReject("errorr1");
                } else {
                    //directionsRenderer.setDirections(response); // Add route to the map
                    var directionsData = response.routes[0].legs[0]; // Get data about the mapped route
                    if (!directionsData) {
                        //window.alert('Directions request failed');
                        //return;
                        myReject("errorr2");
                    }
                    else {
                        returndata += "<div><h6>" + dest.customerName + " : (" + directionsData.distance.text + ", " + directionsData.duration.text + ")</h6><br/>";
                        myResolve(returndata);
                        //alert(" Driving distance is " + directionsData.distance.text + " (" + directionsData.duration.text + ")");
                    }
                }
            });

    }
}
