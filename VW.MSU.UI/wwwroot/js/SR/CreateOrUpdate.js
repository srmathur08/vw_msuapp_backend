﻿var map, marker;

function addDateTimePicker(ele) {
    $(ele).datetimepicker({
        format: 'd/m/Y h:i A',
        formatTime: 'h:i A',
        formatDate: 'd/m/Y',
        ampm: true,
        step: 30,
        validateOnBlur: false,
        onShow: function () {
        }
    }).attr('readonly', 'readonly');
}

function initialLatLng(lat, lng) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': new google.maps.LatLng(lat, lng) }, function (result, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            $('#SearchLocation').val(result[0].formatted_address);
            $("input[name='Lat']").val(result[0].geometry.location.lat());
            $("input[name='Lng']").val(result[0].geometry.location.lng());

            marker.setPosition({
                lat: result[0].geometry.location.lat(),
                lng: result[0].geometry.location.lng()
            });

            marker.setVisible(true);
            map.setZoom(16);
            map.panTo(marker.position);
        }
    });
}

function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        mapTypeControl: false,
        center: { lat: 17.383950000000002, lng: 78.46724 },
        zoom: 11
    });

    var locationInput = document.getElementById("SearchLocation");
    var locationAutocomplete = new google.maps.places.Autocomplete(locationInput);

    setupPlaceChangedListener(locationAutocomplete);

    marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29),
        draggable: true
    });

    marker.setVisible(false);

    marker.addListener("dragend", function (event) {
        $("input[name='Lat']").val(event.latLng.lat());
        $("input[name='Lng']").val(event.latLng.lng());

        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': event.latLng }, function (result, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                $('#SearchLocation').val(result[0].formatted_address);
            }
        });
    });

    if ($("#Lat").val() != '' && $("#Lng").val() != '') {
        initialLatLng($("#Lat").val(), $("#Lng").val());
    }
}

function setupPlaceChangedListener(autocomplete) {
    autocomplete.bindTo('bounds', map);
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }

        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        $("#Lat").val(place.geometry.location.lat());
        $("#Lng").val(place.geometry.location.lng());
    });
}

$(document).ready(function () {
    addDateTimePicker($(".servicedate"));

    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
});

function GetDealershipsByGroupDealer() {
    $("select.dealershipDDL option").remove();
    $("select.dealershipDDL").append($("<option value='0'>Select</option>"));
    var Id = $(".grpDealerDDL option:selected").val();
    $.ajax({
        type: 'POST',
        url: baseUrl + 'Home/GetDealershipsByGroupDealer',
        data: { "Id": parseInt(Id) > 0 ? parseInt(Id) : 0 },
        async: true,
        cache: false,
        success: function (response) {
            debugger;
            for (var i = 0; i < response.length; i++) {
                $("select.dealershipDDL").append($("<option value='" + response[i].id + "'>" + response[i].name + "</option>"))
            };

            $('.selectpicker').selectpicker('refresh');
        },
        complete: function () {

        },
        error: function () {
        }
    });
}