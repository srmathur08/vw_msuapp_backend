//init

//init
const tobii = new Tobii();

//set events
tobii.on("open", function (e) {
    if (e.detail.group === "events") console.log("event: " + "open", e.detail);
});
tobii.on("previous", function (e) {
    if (e.detail.group === "events")
        console.log("event: " + "previous", e.detail);
});
tobii.on("next", function (e) {
    if (e.detail.group === "events") console.log("event: " + "next", e.detail);
});
tobii.on("close", function (e) {
    if (e.detail.group === "events") console.log("event: " + "close", e.detail);
});

new Splide("#splide", {
    type: false,
    // height: "14rem",
    perPage: 1,
    perMove: 1,
    arrows: false,
    pagination: false,
    grid: {
        // You can define rows/cols instead of dimensions.
        rows: 2,
        cols: 2,
        gap: { row: "5px", col: "5px" },
    },
    breakpoints: {
        992: {
            type: "loop",
            perPage: 1,
            padding: {
                right: "8%",
                left: "00px",
            },
            trimSpace: true,
            arrows: true,
            pagination: false,
            grid: {
                rows: 1,
                cols: 1,
            },
        },
    },
}).mount(window.splide.Extensions);

var tabs = document.querySelectorAll(".tabcontent");
var tablinks = document.querySelectorAll(".tablinks");
var body = document.querySelector("body");

// $(window).resize(function () {
// var width = $(window).width();
// if (width <= 902) {
//   console.log("Your screen is too small" + width);
// } else {
//   tabLoaded();
// }
// });

// A $( document ).ready() block.
$(document).ready(function () {
    var width = $(window).width();
    if (width <= 902) {
        console.log("Your screen is too small" + width);
    } else {
        tabLoaded();
    }
});

function tabLoaded() {
    const cityname = localStorage.getItem("cityname");
    tabs.forEach(function (e, i) {
        if (cityname) {
            if (e.id === cityname) {
                e.style.display = "block";
                tablinks[i].className += " active";
            }
        } else if (i == 0) {
            e.style.display = "block";
            tablinks[i].className += " active";
        } else {
            e.style.display = "none";
        }
    });
}

function openData(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;
    localStorage.setItem("cityname", cityName);

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
    body.className = "active";
}

function closeData(evt, cityName) {
    evt.preventDefault();
    body.classList.remove("active");
    document.getElementById(cityName).style.display = "none";
}

$(document).ready(function () {
    // new Splide(".angle-view", {
    //   type: false,
    //   // height: "14rem",
    //   perPage: 1,
    //   perMove: 1,
    //   arrows: false,
    //   pagination: false,
    //   classes: {
    //     arrows: "splide__arrows your-class-arrows",
    //     arrow: "splide__arrow your-class-arrow",
    //     prev: "splide__arrow--prev your-class-prev",
    //     next: "splide__arrow--next your-class-next",
    //   },
    // }).mount(window.splide.Extensions);

    new Splide(".angle-view", {
        pagination: false,
        type: "fade",
        // rewind: true,
        classes: {
            // arrows: "splide__arrows your-class-arrows",
            // arrow: "splide__arrow your-class-arrow",
            prev: "splide__arrow--prev your-class-prev",
            next: "splide__arrow--next your-class-next",
        },
    }).mount();
});
