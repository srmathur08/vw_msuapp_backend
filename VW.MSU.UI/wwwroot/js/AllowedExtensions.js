﻿function allowedextensions(value, element, param) {
    if ($(element)[0].files != null && $(element)[0].files.length > 0) {
        var extensions = $(element).data('val-extensions');
        if (extensions && extensions.length > 0) {
            var extArray = extensions.split(",");
            var fileName = $(element).val().split("\\").pop();
            var fileExt = fileName.substring(fileName.lastIndexOf("."));
            for (var i = 0; i < extArray.length; i++) {
                if (extArray[i] == fileExt)
                    return true;
            }
        }
        return false;
    }
    return true;
}

$.validator.addMethod("allowedextensions", allowedextensions);
$.validator.unobtrusive.adapters.addBool("allowedextensions");