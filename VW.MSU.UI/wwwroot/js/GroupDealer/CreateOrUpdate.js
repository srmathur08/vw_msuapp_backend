﻿var dealerUsers = 0;
$(document).ready(function () {
    dealerUsers = $(".location-add").length;
    document.getElementById("dealer-user-add").addEventListener("click", function (e) {
        var dealerUser = $("<div class=\"location-add  form-row  mb-3 input-row\">" +
                                "<input type=\"hidden\" id=\"GroupDealerUsers_" + dealerUsers + "__Id\" name=\"GroupDealerUsers[" + dealerUsers + "].Id\" value=\"0\" />" +
                                "<div class=\"col-lg-3\">" +
            "<input id=\"GroupDealerUsers_" + dealerUsers + "__Fullname\" name=\"GroupDealerUsers[" + dealerUsers + "].Fullname\" class=\"map-input form-control form-control-custom controls\" type=\"text\" placeholder=\"Fullname\" data-val=\"true\" data-val-required=\"The Fullname is required.\" />" +
                                    "<span class=\"text-danger field-validation-valid\" data-valmsg-for=\"GroupDealerUsers[" + dealerUsers + "].Fullname\" data-valmsg-replace=\"true\"></span>" +
                                "</div>" +
                                "<div class=\"col-lg-3\">" +
            "<input id=\"GroupDealerUsers_" + dealerUsers + "__Email\" name=\"GroupDealerUsers[" + dealerUsers + "].Email\" class=\"map-input form-control form-control-custom controls\" type=\"text\" placeholder=\"Email\" data-val=\"true\" data-val-required=\"The Email is required.\" />" +
                                    "<span class=\"text-danger field-validation-valid\" data-valmsg-for=\"GroupDealerUsers[" + dealerUsers + "].Email\" data-valmsg-replace=\"true\"></span>" +
                                "</div>" +
                                "<div class=\"col-lg-3\">" +
            "<input id=\"GroupDealerUsers_" + dealerUsers + "__Phone\" name=\"GroupDealerUsers[" + dealerUsers + "].Phone\" class=\"map-input form-control form-control-custom controls\" type=\"text\" placeholder=\"Phone Number\" data-val=\"true\" data-val-required=\"The Phone Number is required.\" />" +
                                    "<span class=\"text-danger field-validation-valid\" data-valmsg-for=\"GroupDealerUsers[" + dealerUsers + "].Phone\" data-valmsg-replace=\"true\"></span>" +
                                "</div>" +
                                "<div class=\"col-lg-3\">" +
                                    "<input type=\"checkbox\" style=\"display: none;\" id=\"GroupDealerUsers_" + dealerUsers + "__IsDeleted\" name=\"GroupDealerUsers[" + dealerUsers + "].IsDeleted\" value=\"true\" />" +
                                    "<button type=\"button\" class=\"delete-icon icon action-icon\" onclick=\"removeGroupDealer(this);\"></button>" +
                                "</div>" +
                            "</div>");
        $("#dealer-users-list").append($(dealerUser));

        $("#CreateOrUpdateForm").removeData('validator');
        $("#CreateOrUpdateForm").removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse("#CreateOrUpdateForm");

        dealerUsers++;
    });
});

function removeGroupDealer(obj) {
    var parentEle = $(obj).closest(".location-add");
    $("input[type='checkbox']", $(parentEle)).prop("checked", true);
    $(parentEle).hide();
}