﻿var AutoHeight = function (wrapper, bottomspace) {
    resizes();
    $(window).resize(resizes);
    function resizes() {
        this.bottomspace = bottomspace;
        this.wrapper = wrapper;
        this.Height = function () {
            windowHeight = $(window).height(),
                divOffsetTop = this.wrapper.offset().top,
                divOffsetHeight = this.wrapper.height(),
                bottomSpace = this.bottomspace,
                divHeight = divOffsetTop + divOffsetHeight,
                bottomSpacing = windowHeight - divHeight;
            return (divOffsetHeight + bottomSpacing) - bottomSpace;
        }
        this.wrapper.css({
            'height': this.Height(),
            'max-height': this.Height(),
            'border': 0
        });

    }

}