﻿function VerifyVIN(vin) {
    var res = null;

    $.ajax({
        type: 'POST',
        url: baseUrl + 'Home/VerifyVIN',
        data: { "VIN": vin },
        async: false,
        cache: false,
        success: function (response) {
            res = response;
        },
        complete: function () {

        },
        error: function () {
            
        }
    });

    return res;
}

$(function () {
    $('.hamburger').click(function () {
        $('.navWrapper').fadeIn();
    })

    $('.close-menu').click(function () {
        $('.navWrapper').fadeOut();
    })
});